<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student_payment extends Model
{
   	public function student()
	{
    	return $this->belongsTo('App\Student');
    }
}
