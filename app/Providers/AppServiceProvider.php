<?php

namespace App\Providers;

use Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setLocale('sr');

        view()->composer('layouts.app',
            function ($view) {
                $view->with('events',
                    \App\Event::where('start', '>', \Carbon\Carbon::now())
                        ->orderBy('start', 'asc')
                        ->take(10)
                        ->get());
            });

        view()->composer('layouts.app',
            function ($view) {
                $view->with('msg_cnt', \App\Message::count(Auth::user()->id));
            });

        view()->composer('layouts.app',
            function ($view) {
                $view->with('messages', \App\Message::getUnread(Auth::user()->id));;
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
