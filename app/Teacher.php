<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Teacher extends Model
{
   	public function category()
	{
    	return $this->belongsTo('App\Category');
    }

    public function payments()
    {
        return $this->hasMany('App\Teacher_payment');
    }    

    protected static function boot() 
    {
        parent::boot();

        static::deleting(function($teacher) { 

             DB::table('courses')
            ->where('teacher_id', $teacher->id)
            ->update(['teacher_id' => null]);

        });
    }    
}
