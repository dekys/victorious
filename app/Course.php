<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function teacher()
    {
        return $this->belongsTo('App\Teacher');
    }

	public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

	public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function students()
    {
        return $this->belongsToMany('App\Student')->withPivot('price');
    }


    protected static function boot() 
    {
        parent::boot();

        static::deleting(function($course) { 
             $course->events()->delete();
        });
    }
}
