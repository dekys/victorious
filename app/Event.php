<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['title','start','end'];


    public function course()
    {
        return $this->belongsTo('App\Course');
    }  

    public function students()
    {
    	return $this->belongsToMany('App\Student');
    }  
}
