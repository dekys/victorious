<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
	protected $table = 'categories';

	public function service()
	{
    	return $this->hasMany('App\Service');
    }

	public function teachers()
	{
    	return $this->hasMany('App\Teacher');
    }

    protected static function boot() 
    {
        parent::boot();

        static::deleting(function($category) { 
             $category->service()->delete();

             DB::table('teachers')
            ->where('category_id', $category->id)
            ->update(['category_id' => null]);

        });
    }
}

