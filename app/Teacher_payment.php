<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher_payment extends Model
{
   	public function teacher()
	{
    	return $this->belongsTo('App\Teacher');
    }
}
