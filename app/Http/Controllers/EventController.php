<?php

namespace App\Http\Controllers;

use App\Event;
use App\Student;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event                   = new Event();
        $event->title            = $request->input("title");
        $event->start            = $request->input("start");
        $event->end              = $request->input("end");
        $event->is_all_day       = $request->input("is_all_day");
        $event->background_color = $request->input("background_color");
        $event->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event    = Event::find($id);
        $students = $event->course->students()->paginate(15);
        return view('events.show', compact('students', 'event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event        = Event::findOrFail($id);
        $event->title = $request->input("title");
        $event->start = $request->input("start");
        $event->end   = $request->input("end");
        $event->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function attend(Request $request)
    {
        $student = Student::find($request->student_id);

        if ($request->present == "true") {
            $student->events()->attach($request->event_id);
        } else {
            $student->events()->detach($request->event_id);
        }
    }

}
