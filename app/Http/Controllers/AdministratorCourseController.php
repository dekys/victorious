<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use App\Event;
use App\Location;
use App\Service;
use App\Student;
use App\Teacher;
use Carbon;
use DB;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class AdministratorCourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::paginate(15);
        return view('administrators/courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories     = Category::pluck('naziv', 'id');
        $locations      = Location::pluck('name', 'id');
        $services       = [];
        $course         = [];
        $teachers       = [];
        $payment_method = array(
            '1' => 'Fiksni iznos',
            '2' => 'Procenat',
        );
        // $start_date = null;
        // $end_date = null;
        $start_date = Carbon::Now()->format('d.m.Y.');
        $end_date   = Carbon::Now()->format('d.m.Y.');

        return view('administrators/courses.create')
            ->withCategories($categories)
            ->withServices($services)
            ->withLocations($locations)
            ->withPayment_method($payment_method)
            ->withCourse($course)
            ->withTeachers($teachers)
            ->withStart_date($start_date)
            ->withEnd_date($end_date);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate

        //store
        $course                 = new Course;
        $course->teacher_id     = $request->teacher_id;
        $course->location_id    = $request->location_id;
        $course->service_id     = $request->service_id;
        $course->start_date     = Carbon::parse($request->start_date)->format('Y-m-d');
        $course->start_time     = $request->start_time;
        $course->end_date       = Carbon::parse($request->end_date)->format('Y-m-d');
        $course->end_time       = $request->end_time;
        $course->payment_method = $request->payment_method;
        $course->amount         = $request->amount;
        $course->mon            = $request->mon;
        $course->tue            = $request->tue;
        $course->wed            = $request->wed;
        $course->thu            = $request->thu;
        $course->fri            = $request->fri;
        $course->sat            = $request->sat;
        $course->sun            = $request->sun;
        $course->save();

        $service  = Service::find($request->service_id);
        $category = Category::find($request->category_id);

        //setovanje promenljivih za event
        $title            = $category->naziv . ' - ' . $service->naziv;
        $background_color = $category->color;
        $is_all_day       = 0;

        //kreiranje niza sa rednim brojem dana za koji se kreira dogadjaj
        $days = array();
        if (isset($request->mon)) {
            $days[] = '1';
        };
        if (isset($request->tue)) {
            $days[] = '2';
        };
        if (isset($request->wed)) {
            $days[] = '3';
        };
        if (isset($request->thu)) {
            $days[] = '4';
        };
        if (isset($request->fri)) {
            $days[] = '5';
        };
        if (isset($request->sat)) {
            $days[] = '6';
        };
        if (isset($request->sun)) {
            $days[] = '7';
        };

        // Start date
        $date = Carbon::parse($request->start_date)->format('Y-m-d');
        // End date
        $end_date = Carbon::parse($request->end_date)->format('Y-m-d');

        while (strtotime($date) <= strtotime($end_date)) {
            // trenutni datum se pretvara u redni broj dana u nedelji
            $day = date('N', strtotime($date));
            //provera da li je trenutni redni broj u nedelji planiran ako jeste onda upisuje
            if (in_array($day, $days)) {
                $start = date('Y-m-d H:i:s', strtotime("$date $request->start_time"));
                $end   = date('Y-m-d H:i:s', strtotime("$date $request->end_time"));

                $event                   = new Event();
                $event->course_id        = $course->id;
                $event->title            = $title;
                $event->start            = $start;
                $event->end              = $end;
                $event->is_all_day       = $is_all_day;
                $event->background_color = $background_color;
                $event->save();
            }
            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

        Toastr::info("Novi termin je uspešno unet");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course      = Course::find($id);
        $countEvents = Event::where('course_id', '=', $id)->count();

        return view('administrators/courses.show', compact('course', 'countEvents'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course     = Course::find($id);
        $categories = Category::pluck('naziv', 'id');
        $locations  = Location::pluck('name', 'id');
        $services   = Service::where('category_id', '=', $course->service->category_id)->pluck('naziv', 'id');
        $teachers   = Teacher::select(
            DB::raw("CONCAT(ime,' ',prezime) AS name"), 'id')
            ->where('category_id', '=', $course->service->category_id)
            ->pluck('name', 'id');

        $payment_method = array(
            '1' => 'Fiksni iznos',
            '2' => 'Procenat',
        );
        $start_date = Carbon::parse($course->start_date)->format('d.m.Y.');
        $end_date   = Carbon::parse($course->end_date)->format('d.m.Y.');

        return view('administrators/courses.edit')
            ->withCategories($categories)
            ->withServices($services)
            ->withLocations($locations)
            ->withPayment_method($payment_method)
            ->withCourse($course)
            ->withTeachers($teachers)
            ->withStart_date($start_date)
            ->withEnd_date($end_date);

        // Toastr::info("Podaci o usluzi su uspešno izmenjeni");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate

        //store
        $course                 = Course::find($id);
        $course->teacher_id     = $request->teacher_id;
        $course->location_id    = $request->location_id;
        $course->service_id     = $request->service_id;
        $course->start_date     = Carbon::parse($request->start_date)->format('Y-m-d');
        $course->start_time     = $request->start_time;
        $course->end_date       = Carbon::parse($request->end_date)->format('Y-m-d');
        $course->end_time       = $request->end_time;
        $course->payment_method = $request->payment_method;
        $course->amount         = $request->amount;
        $course->mon            = $request->mon;
        $course->tue            = $request->tue;
        $course->wed            = $request->wed;
        $course->thu            = $request->thu;
        $course->fri            = $request->fri;
        $course->sat            = $request->sat;
        $course->sun            = $request->sun;
        $course->save();

        $service  = Service::find($request->service_id);
        $category = Category::find($request->category_id);

        $event = Event::where('course_id', '=', $course->id);
        $event->delete();
        
        //setovanje promenljivih za event
        $title            = $category->naziv . ' - ' . $service->naziv;
        $background_color = $category->color;
        $is_all_day       = 0;

        //kreiranje niza sa rednim brojem dana za koji se kreira dogadjaj
        $days = array();
        if (isset($request->mon)) {
            $days[] = '1';
        };
        if (isset($request->tue)) {
            $days[] = '2';
        };
        if (isset($request->wed)) {
            $days[] = '3';
        };
        if (isset($request->thu)) {
            $days[] = '4';
        };
        if (isset($request->fri)) {
            $days[] = '5';
        };
        if (isset($request->sat)) {
            $days[] = '6';
        };
        if (isset($request->sun)) {
            $days[] = '7';
        };

        // Start date
        $date = Carbon::parse($request->start_date)->format('Y-m-d');
        // End date
        $end_date = Carbon::parse($request->end_date)->format('Y-m-d');

        while (strtotime($date) <= strtotime($end_date)) {
            // trenutni datum se pretvara u redni broj dana u nedelji
            $day = date('N', strtotime($date));
            //provera da li je trenutni redni broj u nedelji planiran ako jeste onda upisuje
            if (in_array($day, $days)) {
                $start = date('Y-m-d H:i:s', strtotime("$date $request->start_time"));
                $end   = date('Y-m-d H:i:s', strtotime("$date $request->end_time"));

                $event                   = new Event();
                $event->course_id        = $course->id;
                $event->title            = $title;
                $event->start            = $start;
                $event->end              = $end;
                $event->is_all_day       = $is_all_day;
                $event->background_color = $background_color;
                $event->save();
            }
            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

        Toastr::info("Termin je uspešno izmenjen");
        // return back();
        return redirect()->route('course.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        $course->delete();

        Toastr::info("Termin je uspešno izbrisan");

        //redirect
        return back();
    }

    public function changePrice(Request $request)
    {
        $student = Student::find($request->student_id);
        $student->courses()->updateExistingPivot($request->course_id, ['price' => intval($request->amount)]);
        Toastr::info("Cena je uspešno promenjena");
        return redirect()->route('course.show', $request->course_id);
    }

    public function findServices(Request $request)
    {
        $services = Service::select('naziv', 'id')->where('category_id', $request->id)->get();
        $teachers = Teacher::select('ime', 'prezime', 'id')->where('category_id', $request->id)->get();
        return response()->json(["services" => $services, "teachers" => $teachers]); //then sent this data to ajax success
    }

    public function findStudent(Request $request)
    {
        $course   = Course::find($request->course_id);
        $students = Student::orderBy('prezime', 'asc')->get();
        $students = $students->diff($course->students);

        return view('administrators/courses.findStudent', compact('students', 'course'));
    }

    public function addStudent($id, $course_id)
    {
        $student = Student::find($id);
        $course  = Course::find($course_id);
        $student->courses()->attach($course_id, ['price' => $course->service->cena]);
        Toastr::info("Novi polaznik je uspešno pridružen");
        return redirect()->route('course.show', $course_id);
    }

    public function deleteStudent($id, $course_id)
    {
        $student = Student::find($id);
        $student->courses()->detach($course_id);
        Toastr::info("Polaznik je uspešno uklonjen iz grupe");
        return back();
    }
}
