<?php

namespace App\Http\Controllers;

use App\Category;
use App\Teacher;
use App\User;
use Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use Mail;
use PDF;

class AdministratorTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->get('q');

        if ($q==null) {
            $teachers = Teacher::orderBy('prezime', 'asc')->paginate(15);
        } else {
            $teachers = Teacher::orderBy('prezime', 'asc')
                ->where('ime', 'like', ('%'.$q.'%'))
                ->orwhere('prezime', 'like', ('%'.$q.'%'))
                ->paginate(15);
        }
        return view('administrators/teachers.index', compact('teachers', 'q'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Category::pluck('naziv', 'id');
        $datum    = null;
        return view('administrators/teachers.create')
            ->withServices($services)
            ->withDatum($datum);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate

        //store
        $teacher              = new Teacher;
        $teacher->ime         = $request->ime;
        $teacher->prezime     = $request->prezime;
        $teacher->rodjen      = Carbon::parse($request->rodjen)->format('Y-m-d');
        $teacher->adresa      = $request->adresa;
        $teacher->telefon     = $request->telefon;
        $teacher->email       = $request->email;
        $teacher->jmbg        = $request->jmbg;
        $teacher->brlk        = $request->brlk;
        $teacher->pu          = $request->pu;
        $teacher->biografija  = $request->biografija;
        $teacher->category_id = $request->category_id;
        $teacher->vocation    = $request->vocation;
        $teacher->school      = $request->school;
        $teacher->address     = $request->address;
        $teacher->percent     = $request->percent;

        $teacher->save();

        // Ako je obelezeno da kreira novog korisnika kod kreiranja
        // polaznika
        if (isset($request->create_user)) {
            $rand_pwdom = str_random(10);

            $user            = new User;
            $user->firstname = $request->ime;
            $user->lastname  = $request->prezime;
            $user->email     = $request->email;
            $user->password  = bcrypt($rand_pwdom);
            $user->save();

            $role_id = 2;
            $user->roles()->attach($role_id);

            /*Priprema i slanje e-maila*/
            Mail::send('administrators/teachers.email.registration', ['user' => $user, 'pwd' => $rand_pwdom], function ($message) use ($user) {
                $message->from('app@victorious.co.rs', 'Victorious');
                $message->to($user->email, $user->firstname);
                $message->subject('Registracija korisnika');
            });

            Toastr::info("Novi korisnik sistema je uspesno kreiran. Poslat e-mail.");
        }

        Toastr::info("Novi predavač je uspešno unesen");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::find($id);
        return view('administrators/teachers.show')->withTeacher($teacher);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teachers = Teacher::find($id);
        $services = Category::pluck('naziv', 'id');
        $datum    = Carbon::parse($teachers->rodjen)->format('d.m.Y');

        return view('administrators/teachers.edit')
            ->withTeachers($teachers)
            ->withServices($services)
            ->withDatum($datum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //update
        $teacher              = Teacher::find($id);
        $teacher->ime         = $request->ime;
        $teacher->prezime     = $request->prezime;
        $teacher->rodjen      = Carbon::parse($request->rodjen)->format('Y-m-d');
        $teacher->adresa      = $request->adresa;
        $teacher->telefon     = $request->telefon;
        $teacher->email       = $request->email;
        $teacher->jmbg        = $request->jmbg;
        $teacher->brlk        = $request->brlk;
        $teacher->pu          = $request->pu;
        $teacher->biografija  = $request->biografija;
        $teacher->category_id = $request->category_id;
        $teacher->vocation    = $request->vocation;
        $teacher->school      = $request->school;
        $teacher->address     = $request->address;
        $teacher->percent     = $request->percent;
        $teacher->save();

        Toastr::info("Podaci o predavaču su uspešno izmenjeni");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teachers = Teacher::find($id);
        $teachers->delete();

        Toastr::info("Predavač je uspešno izbrisan");

        //redirect
        return redirect()->route('teacher.index');
    }

    public function PdfSpisakPredavaca()
    {
        $teachers     = Teacher::orderBy('prezime', 'asc')->get();
        $sum_teachers = count($teachers);

        $pdf = PDF::loadView('administrators/teachers/pdf/pdf_spisak', ['teachers' => $teachers, 'sum_teachers' => $sum_teachers]);
        return $pdf->download('spisak_predavaca.pdf');
    }

    public function PdfPrijava($id)
    {
        Carbon::setToStringFormat('d.m.Y.');
        $teachers = Teacher::find($id);
        $date     = Carbon::now();

        $pdf = PDF::loadView('administrators/teachers/pdf/pdf_prijava', ['teachers' => $teachers, 'date' => $date])->setPaper('a4');
        return $pdf->download('Prijava_' . $teachers->ime . '_' . $teachers->prezime . '.pdf');
    }

    public function PdfUgovor(Request $request)
    {
        // dd($request);
        Carbon::setToStringFormat('d.m.Y.');
        $teachers            = Teacher::find($request->teacher_id);
        $teachers->date_from = $request->date_from;
        $teachers->date_to   = $request->date_to;
        $teachers->cena      = $request->cena;
        $date                = Carbon::now();

        $pdf = PDF::loadView('administrators/teachers/pdf/pdf_ugovor', ['teachers' => $teachers, 'date' => $date])->setPaper('a4');
        return $pdf->download('Ugovor_' . $teachers->ime . '_' . $teachers->prezime . '.pdf');
    }
}
