<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::orderBy('name', 'asc')->paginate(15);
        return view('administrators/locations.index')->withLocations($locations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrators/locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate

        //store
        $location           = new Location;
        $location->name     = $request->name;
        $location->capacity = $request->capacity;
        $location->note     = $request->note;
        $location->address  = $request->address;

        $location->save();

        Toastr::info("Nova lokacija je uspešno uneta");

        //redirect
        return redirect()->route('location.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::findorfail($id);
        return view('administrators/locations.edit')->withLocation($location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location           = Location::findorfail($id);
        $location->name     = $request->name;
        $location->capacity = $request->capacity;
        $location->note     = $request->note;
        $location->address  = $request->address;

        $location->save();

        Toastr::info("Lokacija je uspešno izmenjena");

        //redirect
        return redirect()->route('location.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::findorfail($id);
        $location->delete();

        Toastr::info("Lokacija je uspešno izbrisana");

        //redirect
        return redirect()->route('location.index');
    }
}
