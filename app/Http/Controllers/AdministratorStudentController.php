<?php

namespace App\Http\Controllers;

use App\Parents;
use App\Student;
use App\Subject;
use App\User;
use Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use Mail;
use PDF;

class AdministratorStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->get('q');

        if ($q==null) {
            $students = Student::orderBy('prezime', 'asc')->paginate(15);
        } else {
            $students = Student::orderBy('prezime', 'asc')
                ->where('ime', 'like', ('%'.$q.'%'))
                ->orwhere('prezime', 'like', ('%'.$q.'%'))
                ->paginate(15);
        }

        return view('administrators/students.index', compact('students', 'q'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datum = null;
        return view('administrators/students.create')->withDatum($datum);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        // dd($request);
        //store
        $parent                = new Parents();
        $parent->ime_oca       = $request->parent['ime_oca'];
        $parent->prezime_oca   = $request->parent['prezime_oca'];
        $parent->telefon_oca   = $request->parent['telefon_oca'];
        $parent->email_oca     = $request->parent['email_oca'];
        $parent->ime_majke     = $request->parent['ime_majke'];
        $parent->prezime_majke = $request->parent['prezime_majke'];
        $parent->telefon_majke = $request->parent['telefon_majke'];
        $parent->email_majke   = $request->parent['email_majke'];
        $parent->save();

        $student          = new Student();
        $student->ime     = $request->ime;
        $student->prezime = $request->prezime;
        $student->rodjen  = Carbon::parse($request->rodjen)->format('Y-m-d');
        $student->adresa  = $request->adresa;
        $student->telefon = $request->telefon;
        $student->email   = $request->email;

        if ($request->person == 0) {
            $student->razred = $request->razred;
            $student->skola  = $request->skola;
        } else {
            $student->jmbg = $request->jmbg;
            $student->brlk = $request->brlk;
            $student->pu   = $request->pu;
        }

        $student->napomena = $request->napomena;
        $student->person   = $request->person;

        $student->parent()->associate($parent);

        $student->save();

        // Ako je obelezeno da kreira novog korisnika kod kreiranja
        // polaznika
        if (isset($request->create_user)) {
            $rand_pwdom = str_random(10);

            $user            = new User;
            $user->firstname = $request->ime;
            $user->lastname  = $request->prezime;
            $user->email     = $request->email;
            $user->password  = bcrypt($rand_pwdom);
            $user->save();

            $role_id = 4;
            $user->roles()->attach($role_id);

            /*Priprema i slanje e-maila*/
            Mail::send('administrators/students.email.registration', ['user' => $user, 'pwd' => $rand_pwdom], function ($message) use ($user) {
                $message->from('app@victorious.co.rs', 'Victorious');
                $message->to($user->email, $user->firstname);
                $message->subject('Registracija korisnika');
            });

            Toastr::info("Novi korisnik sistema je uspesno kreiran. Poslat e-mail.");
        }

        Toastr::info("Novi polaznik je uspešno unesen");

        //redirect
        $student = Student::all()->last(); // last element
        // return view('administrators/students.show')->withStudent($student);
        return redirect()->route('student.show', [$student->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);

        return view('administrators/students.show')->withStudent($student);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Student::find($id);
        $datum    = Carbon::parse($students->rodjen)->format('d.m.Y');

        return view('administrators/students.edit')->withStudents($students)->withDatum($datum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);

        $student->ime     = $request->ime;
        $student->prezime = $request->prezime;
        $student->rodjen  = Carbon::parse($request->rodjen)->format('Y-m-d');
        $student->adresa  = $request->adresa;
        $student->telefon = $request->telefon;
        $student->email   = $request->email;

        if ($request->person == 0) {
            $student->razred = $request->razred;
            $student->skola  = $request->skola;
        } else {
            $student->jmbg = $request->jmbg;
            $student->brlk = $request->brlk;
            $student->pu   = $request->pu;
        }

        $student->napomena = $request->napomena;
        $student->person   = $request->person;

        $student->parent->ime_oca       = $request->parent['ime_oca'];
        $student->parent->prezime_oca   = $request->parent['prezime_oca'];
        $student->parent->telefon_oca   = $request->parent['telefon_oca'];
        $student->parent->email_oca     = $request->parent['email_oca'];
        $student->parent->ime_majke     = $request->parent['ime_majke'];
        $student->parent->prezime_majke = $request->parent['prezime_majke'];
        $student->parent->telefon_majke = $request->parent['telefon_majke'];
        $student->parent->email_majke   = $request->parent['email_majke'];

        $student->push();

        return view('administrators/students.show')->withStudent($student);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $students = Student::find($id);
        $students->delete();

        Toastr::info("Polaznik je uspešno izbrisan");

        //redirect
        return back();
    }

    public function subject_destroy($id)
    {
        $subject = Subject::find($id);
        $subject->delete();
    }

    public function subject_store(Request $request)
    {
        $subject             = new Subject;
        $subject->name       = $request['name'];
        $subject->student_id = $request['id'];
        $subject->teacher    = $request['teacher'];
        $subject->mark       = $request['mark'];
        $subject->comment    = $request['comment'];
        $subject->save();
    }

    public function subject_update(Request $request, $id)
    {
        $subject          = Subject::find($id);
        $subject->name    = $request['name'];
        $subject->teacher = $request['teacher'];
        $subject->mark    = $request['mark'];
        $subject->comment = $request['comment'];
        $subject->push();
    }

    public function PdfPrijava($id)
    {
        Carbon::setToStringFormat('d.m.Y.');
        $student = Student::find($id);
                    
        if ($student->person == 0) {
            $pdf     = PDF::loadView('administrators/students/pdf/pdf_prijava_predmet_deca', ['student' => $student])->setPaper('a4');
        } else {
            $pdf     = PDF::loadView('administrators/students/pdf/pdf_prijava_predmet_odrasli', ['student' => $student])->setPaper('a4');
        }

        return $pdf->download('Prijava_za_predmet_' . $student->ime . '_' . $student->prezime . '.pdf');
    }
}
