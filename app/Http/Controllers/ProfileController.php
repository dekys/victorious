<?php

namespace App\Http\Controllers;

use App\Role;
use DB;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use Mail;
use Kamaln7\Toastr\Facades\Toastr;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $profile = Auth::user();
        $rol = Role::all();

        return view('profile.index', compact('profile', 'rol'));
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rol = Role::all();
        return view('profile.create',  compact('rol') );
    }


    public function edit(User $profile)
    {
        $rol = Role::all();
        return view('profile.index', compact('profile', 'rol'));
    }

    public function update(User $profile, Request $request)
    {
        $this->validate($request, [
            "firstname" => "required",
            "lastname"  => "required",
            "image"     => "required",
        ]);

        $profile->update($request->all());
        Toastr::info("Vaš profil je uspešno promenjen");
        return back();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "firstname" => "required",
            "lastname"  => "required",
            "email"     => "required|unique:users"
        ]);

        $rand_pwdom = str_random(10);

        $user            = new User;
        $user->firstname = $request->firstname;
        $user->lastname  = $request->lastname;
        $user->email     = $request->email;
        $user->password  = bcrypt($rand_pwdom);
        $user->save();

        $user->attachRole($request->role);

        Mail::send('administrators/teachers.email.registration', ['user' => $user, 'pwd' => $rand_pwdom], function ($message) use ($user) {
                $message->from('app@victorious.co.rs', 'Victorious');
                $message->to($user->email, $user->firstname);
                $message->subject('Registracija korisnika');
            });

        Toastr::info("Novi korisnik sistema je uspesno kreiran. Poslat e-mail.");

        return redirect('/admin');
    }

    public function update_password(User $profile, Request $request)
    {

        $this->validate($request, [
            'password' => 'required|min:6|confirmed',
        ]);

        $profile->update([
            'password' => bcrypt($request->password),
        ]);

        Toastr::info("Vaša lozinka je uspešno promenjena");
        return back();
    }

    public function update_avatar(User $profile, Request $request)
    {
        // Handle the user upload of avatar
        if ($request->hasFile('avatar')) {
            $user     = User::find($profile->id);
            $avatar   = $request->file('avatar');
            $filename = $user->id . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('/img/' . $filename));
            $user->image = 'img/' . $filename;
            $user->save();
        }

        Toastr::info("Avatar je uspešno zamenjen");
        return back();
    }

    public function UpdateRoles(Request $request, $id)
    {
        $profile = User::find($id);

        // obrisi sve uloge i dodaj mu zadatu
        DB::table('role_user')->where('user_id', '=', $id)->delete();
        $profile->attachRole($request->role);

        return back();

    }

    public function destroy(User $profile)
    {
        $profile->delete();
        return back();
    }

}
