<?php

namespace App\Http\Controllers;

use App\Event;
use App\Location;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($location_id)
    {

        $events = Event::whereHas('course', function ($query) use ($location_id) {
            $query->where('location_id', '=', $location_id);
        })->with('course.location', 'course.teacher')->get();

        $locations = Location::all();


        return view('dashboard', compact('events', 'locations'));

    }

}
