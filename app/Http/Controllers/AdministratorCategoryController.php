<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Toastr;

class AdministratorCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('naziv', 'asc')->paginate(15);

        return view('administrators/services.category_index')->withCategories($categories);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.a
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category        = new Category;
        $category->naziv = $request->naziv;
        $category->opis  = $request->opis;
        $category->color = $request->color;

        $category->Save();

        Toastr::info("Nova kategorija je uspešno upisana");

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return view('administrators/services.category_index')->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate
        // $this->validate($request, array(
        //     'naziv' => 'required|min:5|max:50|unique:categories,naziv,' . $request->naziv,
        //     'opis'  => 'max:50',
        // ));

        $category = Category::findorfail($request->id);

        $category->naziv = $request->naziv;
        $category->opis  = $request->opis;
        $category->color = $request->color;
        
        $category->Save();

        Toastr::info("Kategorija je uspešno izmenjena");

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        Toastr::info("Kategorija je uspešno izbrisana");

        return back();
    }
}
