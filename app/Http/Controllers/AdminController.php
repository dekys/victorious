<?php

namespace App\Http\Controllers;

use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $q = $request->get('q');

        // if ($q==null) {
        $users = User::orderBy('firstname', 'asc')->paginate(15);
        // } else {
        //     $services = Service::orderBy('firstname', 'asc')
        //         ->where('firstname', 'like', ('%'.$q.'%'))
        //         ->orwhere('lastname', 'like', ('%'.$q.'%'))
        //         ->paginate(15);
        // }    

        return view('admin.index', compact('users'));
    }

    public function edit_user(User $profile)
    {
        return view('admin.index', compact('users'));
    }
}
