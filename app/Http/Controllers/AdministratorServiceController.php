<?php

namespace App\Http\Controllers;

use App\Category;
use App\Service;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class AdministratorServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->get('q');

        if ($q==null) {
            $services = Service::orderBy('naziv', 'asc')->paginate(15);
        } else {
            $services = Service::orderBy('naziv', 'asc')
                ->where('naziv', 'like', ('%'.$q.'%'))
                ->paginate(15);
        }        

        $categories = Category::pluck('naziv', 'id');
        
        return view('administrators/services.index', compact('services', 'categories', 'q'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //usluge u select
        $categories = Category::pluck('naziv', 'id');
        return view('administrators/services.create')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        // dd($request);

        //store
        $service              = new Service;
        $service->naziv       = $request->naziv;
        $service->opis        = $request->opis;
        $service->cena        = $request->cena;
        $service->trajanje    = $request->trajanje;
        $service->jm          = $request->jm;
        $service->category_id = $request->category_id;

        $service->save();

        Toastr::info("Nova usluga je uspešno uneta");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::pluck('naziv', 'id');
        $services   = Service::find($id);
        return view('administrators/services.edit')->withServices($services)->withCategories($categories);

        Toastr::info("Podaci o usluzi su uspešno izmenjeni");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service              = Service::find($id);
        $service->naziv       = $request->naziv;
        $service->opis        = $request->opis;
        $service->cena        = $request->cena;
        $service->trajanje    = $request->trajanje;
        $service->jm          = $request->jm;
        $service->category_id = $request->category_id;

        $service->save();

        Toastr::info("Nova usluga je uspešno izmenjena");
        // return back();
        //redirect
        return redirect()->route('service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $services = Service::find($id);
        $services->delete();

        Toastr::info("Polaznik je uspešno izbrisan");

        //redirect
        return redirect()->route('service.index');
    }
}
