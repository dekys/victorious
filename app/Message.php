<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }

    public static function count($id)
    {
        // cupa BROJ neprocitanih poruka za korisnika sa $id

        $msg_cnt = DB::table('messages')
            ->select(DB::raw('count(*) as msg_cnt'))
            ->where('unread', '=', 1)
            ->where('receiver_id', '=', $id)
            ->first()
            ->msg_cnt;

        return $msg_cnt;
    }

    public static function getUnread($id)
    {
        // cupa sve neprocitane poruke za korisnika sa $id

        $messages = Message::where('unread', '=', 1)
            ->where('receiver_id', '=', $id)
            ->latest()
            ->get();

        return $messages;
    }
}
