<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Klaravel\Ntrust\Traits\NtrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use NtrustUserTrait; // add this trait to your user model



    /*
     * Role profile to get value from ntrust config file.
     */
    protected static $roleProfile = 'user';


    // public function roles()
    // {
    //     return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    // }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'image',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
