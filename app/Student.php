<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function parent()
    {
		return $this->belongsTo('App\Parents');
    }

    public function subjects()
    {
		return $this->hasMany('App\Subject');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course')->withPivot('price');
    }    

    public function events()
    {
        return $this->belongsToMany('App\Event');
    }

    public function payments()
    {
        return $this->hasMany('App\Student_payment');
    }    
}
