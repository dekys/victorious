-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2017 at 10:02 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `viktorius`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `naziv` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `opis` text COLLATE utf8_unicode_ci NOT NULL,
  `color` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `naziv`, `opis`, `color`, `created_at`, `updated_at`) VALUES
(6, 'Dnevni boravak', '', '#f071ba', '2017-10-15 14:48:52', '2017-10-15 14:48:52'),
(7, 'tet kat', 'qwd', '#000000', '2017-10-17 16:16:01', '2017-10-17 16:16:01'),
(8, 'Neka kategorija', 'Opis neke kategorije', '#00ffff', '2017-10-29 17:11:11', '2017-10-29 17:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `mon` tinyint(1) DEFAULT NULL,
  `tue` tinyint(1) DEFAULT NULL,
  `wed` tinyint(1) DEFAULT NULL,
  `thu` tinyint(1) DEFAULT NULL,
  `fri` tinyint(1) DEFAULT NULL,
  `sat` tinyint(1) DEFAULT NULL,
  `sun` tinyint(1) DEFAULT NULL,
  `payment_method` int(11) NOT NULL,
  `amount` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `teacher_id`, `location_id`, `service_id`, `start_date`, `end_date`, `start_time`, `end_time`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `sun`, `payment_method`, `amount`, `created_at`, `updated_at`) VALUES
(2, 3, 2, 4, '2017-10-23', '2017-10-28', '08:00:00', '12:00:00', 1, 1, 1, 1, 1, NULL, NULL, 1, 20, '2017-10-15 14:51:06', '2017-10-28 19:27:41');

-- --------------------------------------------------------

--
-- Table structure for table `course_student`
--

CREATE TABLE `course_student` (
  `course_id` int(11) NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `course_student`
--

INSERT INTO `course_student` (`course_id`, `student_id`, `price`) VALUES
(2, 1, 104),
(2, 2, 500);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `is_all_day` tinyint(1) NOT NULL,
  `background_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `start`, `end`, `is_all_day`, `background_color`, `course_id`, `created_at`, `updated_at`) VALUES
(24, 'Dnevni boravak - test1', '2017-10-24 07:00:00', '2017-10-24 08:30:00', 0, '#f071ba', 3, '2017-10-24 17:44:13', '2017-10-24 17:44:13'),
(25, 'Dnevni boravak - test1', '2017-10-25 07:00:00', '2017-10-25 08:30:00', 0, '#f071ba', 3, '2017-10-24 17:44:13', '2017-10-24 17:44:13'),
(26, 'Dnevni boravak - test1', '2017-10-23 07:00:00', '2017-10-23 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(27, 'Dnevni boravak - test1', '2017-10-24 07:00:00', '2017-10-24 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(28, 'Dnevni boravak - test1', '2017-10-25 07:00:00', '2017-10-25 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(29, 'Dnevni boravak - test1', '2017-10-26 07:00:00', '2017-10-26 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(30, 'Dnevni boravak - test1', '2017-10-27 07:00:00', '2017-10-27 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(31, 'Dnevni boravak - test1', '2017-10-28 07:00:00', '2017-10-28 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(32, 'Dnevni boravak - test1', '2017-10-30 07:00:00', '2017-10-30 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(33, 'Dnevni boravak - test1', '2017-10-31 07:00:00', '2017-10-31 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(34, 'Dnevni boravak - test1', '2017-11-01 07:00:00', '2017-11-01 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(35, 'Dnevni boravak - test1', '2017-11-02 07:00:00', '2017-11-02 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(36, 'Dnevni boravak - test1', '2017-11-03 07:00:00', '2017-11-03 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(37, 'Dnevni boravak - test1', '2017-11-04 07:00:00', '2017-11-04 08:30:00', 0, '#f071ba', 4, '2017-10-24 17:46:46', '2017-10-24 17:46:46'),
(38, 'Dnevni boravak - test1', '2017-10-25 07:00:00', '2017-10-25 11:30:00', 0, '#f071ba', 5, '2017-10-24 17:51:38', '2017-10-24 17:51:38'),
(39, 'Dnevni boravak - test1', '2017-10-27 07:00:00', '2017-10-27 11:30:00', 0, '#f071ba', 5, '2017-10-24 17:51:38', '2017-10-24 17:51:38'),
(40, 'Dnevni boravak - test1', '2017-10-30 07:00:00', '2017-10-30 11:30:00', 0, '#f071ba', 5, '2017-10-24 17:51:38', '2017-10-24 17:51:38'),
(41, 'Dnevni boravak - test1', '2017-11-01 07:00:00', '2017-11-01 11:30:00', 0, '#f071ba', 5, '2017-10-24 17:51:38', '2017-10-24 17:51:38'),
(42, 'Dnevni boravak - test1', '2017-11-03 07:00:00', '2017-11-03 11:30:00', 0, '#f071ba', 5, '2017-10-24 17:51:38', '2017-10-24 17:51:38'),
(49, 'Dnevni boravak - test1', '2017-10-23 08:00:00', '2017-10-23 12:00:00', 0, '#f071ba', 2, '2017-10-28 19:27:41', '2017-10-28 19:27:41'),
(50, 'Dnevni boravak - test1', '2017-10-24 08:00:00', '2017-10-24 12:00:00', 0, '#f071ba', 2, '2017-10-28 19:27:41', '2017-10-28 19:27:41'),
(51, 'Dnevni boravak - test1', '2017-10-25 08:00:00', '2017-10-25 12:00:00', 0, '#f071ba', 2, '2017-10-28 19:27:41', '2017-10-28 19:27:41'),
(52, 'Dnevni boravak - test1', '2017-10-26 08:00:00', '2017-10-26 12:00:00', 0, '#f071ba', 2, '2017-10-28 19:27:41', '2017-10-28 19:27:41'),
(53, 'Dnevni boravak - test1', '2017-10-27 08:00:00', '2017-10-27 12:00:00', 0, '#f071ba', 2, '2017-10-28 19:27:41', '2017-10-28 19:27:41');

-- --------------------------------------------------------

--
-- Table structure for table `event_student`
--

CREATE TABLE `event_student` (
  `event_id` int(10) NOT NULL,
  `student_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_student`
--

INSERT INTO `event_student` (`event_id`, `student_id`) VALUES
(53, 1),
(53, 2),
(51, 1),
(52, 2),
(49, 2);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `capacity` int(5) NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `capacity`, `note`, `address`, `created_at`, `updated_at`) VALUES
(2, 'Lokacija 1', 10, '', 'Neka ulica br.5', '2017-10-15 13:52:10', '2017-10-15 13:52:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_14_184604_user_ntrust_setup_tables', 2),
(4, '2017_01_17_082137_create_students_table', 3),
(5, '2017_01_22_101507_create_teachers_table', 4),
(6, '2017_01_22_112823_create_services_table', 5),
(8, '2017_01_22_204920_create_calendar_events_table', 6),
(9, '2016_06_01_000001_create_oauth_auth_codes_table', 7),
(10, '2016_06_01_000002_create_oauth_access_tokens_table', 7),
(11, '2016_06_01_000003_create_oauth_refresh_tokens_table', 7),
(12, '2016_06_01_000004_create_oauth_clients_table', 7),
(13, '2016_06_01_000005_create_oauth_personal_access_clients_table', 7),
(14, '2017_08_15_155525_create_events_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` int(10) UNSIGNED NOT NULL,
  `ime_oca` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prezime_oca` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefon_oca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_oca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ime_majke` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prezime_majke` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefon_majke` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_majke` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`id`, `ime_oca`, `prezime_oca`, `telefon_oca`, `email_oca`, `ime_majke`, `prezime_majke`, `telefon_majke`, `email_majke`, `created_at`, `updated_at`) VALUES
(1, 'otac ', 'nikolin', '', '', '', '', '', '', '2017-10-28 19:48:41', '2017-10-28 19:48:41'),
(2, '', '', '', '', '', '', '', '', '2017-10-29 10:26:43', '2017-10-29 10:26:43');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('dejan.stevanovic@hotmail.com', 'ed30cf667b841069c5c3400c6fc65838485502f641672c0d0cce457bcac29389', '2017-08-29 16:50:30');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', 'Administrator ima sva prava', NULL, NULL),
(2, 'teacher', 'Nastavnik', 'Nastavnik ili predavač', NULL, NULL),
(3, 'parent', 'Roditelj', 'Roditelj', NULL, NULL),
(4, 'student', 'Učenik - polaznik', 'Učenik ili polaznik', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(2, 1),
(49, 2),
(50, 4);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) NOT NULL,
  `naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `opis` text COLLATE utf8_unicode_ci NOT NULL,
  `cena` double(8,2) NOT NULL,
  `trajanje` int(11) NOT NULL,
  `jm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `category_id`, `naziv`, `opis`, `cena`, `trajanje`, `jm`, `created_at`, `updated_at`) VALUES
(2, 6, 'Dopunska nastava iz matematike 7. razred', '', 1000.00, 5, 'casova', '2017-10-15 16:59:39', '2017-10-15 20:45:23'),
(4, 6, 'test1', '', 100.00, 1, 'sat', '2017-10-17 16:41:33', '2017-10-17 16:41:33'),
(5, 7, 'opet test', '', 234.00, 2, '234', '2017-10-17 16:42:08', '2017-10-17 16:42:08');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `ime` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prezime` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rodjen` date DEFAULT NULL,
  `adresa` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `razred` int(11) UNSIGNED DEFAULT NULL,
  `skola` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `napomena` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `parent_id`, `ime`, `prezime`, `rodjen`, `adresa`, `telefon`, `email`, `razred`, `skola`, `napomena`, `created_at`, `updated_at`) VALUES
(1, 1, 'Nikola', 'novi', '2017-10-21', 'asdf', 'asdf', 'qswd@gmail.com', 6, 'OŠ jovan  popovic', '', '2017-10-28 19:48:41', '2017-10-28 19:48:41'),
(2, 2, 'anja', 'Stevanovic', '2017-10-19', 'Nenada Lukica 18', '654654', 'email@emrfailnikola.com', 3, 'OŠ jovan  popovic', '', '2017-10-29 10:26:43', '2017-10-29 10:26:43');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) NOT NULL,
  `student_id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teacher` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mark` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `ime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prezime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rodjen` date NOT NULL,
  `adresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jmbg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brlk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `biografija` text COLLATE utf8_unicode_ci NOT NULL,
  `vocation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `school` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percent` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `category_id`, `ime`, `prezime`, `rodjen`, `adresa`, `telefon`, `email`, `jmbg`, `brlk`, `pu`, `biografija`, `vocation`, `school`, `address`, `percent`, `created_at`, `updated_at`) VALUES
(3, 6, 'Dejan', 'Stevanovic', '2017-10-06', 'Slovenska 68, 37000 Kruševac', '54165469584', 'email123@email.com', '3213213213213', '4565321', 'Kruševac', '', 'Profesor tehnologije', 'OŠ \"Jovan Jovanović Zmaj\" Kruševac', '234', '100%', '2017-10-15 15:21:39', '2017-10-24 17:54:19');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_payments`
--

CREATE TABLE `teacher_payments` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `income` float DEFAULT NULL,
  `outcome` float DEFAULT NULL,
  `date` datetime NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teacher_payments`
--

INSERT INTO `teacher_payments` (`id`, `teacher_id`, `description`, `income`, `outcome`, `date`, `create_at`, `update_at`) VALUES
(1, 3, 'Odrzana obuka 5 djaka', 1000, NULL, '2017-10-10 00:00:00', '2017-10-29 20:56:40', '0000-00-00 00:00:00'),
(2, 3, 'Isplata honorara', NULL, 1000, '2017-10-12 00:00:00', '2017-10-29 20:57:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/img/avatar.png',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Dejan', 'Stevanović', 'dejan.stevanovic@hotmail.com', '$2y$10$NSkbGqqm5SnWbud5J.pYbeK5CZt3R/B0ecCnr4miZgRp9tFkDCa9i', 'img/2.jpg', 'nmzrkz0p45Mrn55BLoNI8sgHKCn9de0CEhTraklXif3x4D9AceNyJJbukY8S', '2017-01-15 08:02:42', '2017-10-24 16:04:18'),
(49, 'Jelena', 'Stevanovic', 'jelena.radisavljevic@hotmail.rs', '$2y$10$BFSwAb.SjYXMZPDfQ/Za8.GoLxW1Or96EdQp2v3mSN2mJ1T7OBvWq', 'img/49.jpg', 'XI0ZzKBmvEwljUPcKvFRdFJkf3N9RCHHpuXEnwSvGjUKMSF5l2iZZ3cxjnJC', '2017-10-04 19:38:14', '2017-10-24 16:10:47'),
(50, 'Nikola', 'novi', 'qswd@gmail.com', '$2y$10$6KR1Y88mvHp6gYf8K/H1P.e0l/iYJCOW1AdCjSScXlseRP.Xrq1e6', '/img/avatar.png', NULL, '2017-10-28 19:48:41', '2017-10-28 19:48:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `naziv` (`naziv`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_student`
--
ALTER TABLE `course_student`
  ADD PRIMARY KEY (`course_id`,`student_id`),
  ADD KEY `fk_courses_students_students1_idx` (`student_id`),
  ADD KEY `fk_courses_students_courses1_idx` (`course_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `naziv` (`naziv`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_email_unique` (`email`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `students_id` (`student_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `teacher_payments`
--
ALTER TABLE `teacher_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `teacher_payments`
--
ALTER TABLE `teacher_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
