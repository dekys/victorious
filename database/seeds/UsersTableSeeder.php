<?php

use Illuminate\Database\Seeder;
use \Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('');
        $this->command->info('Creating Admin Role');
        DB::table('roles')->insert([
            'name'         => 'admin',
            'display_name' => 'Administrator',
            'description'  => 'Administrator ima sva prava',
        ]);

        $this->command->info('Creating Teacher Role');
        DB::table('roles')->insert([
            'name'         => 'teacher',
            'display_name' => 'Nastavnik',
            'description'  => 'Nastavnik ili predavač',
        ]);

        $this->command->info('Creating Parent Role');
        DB::table('roles')->insert([
            'name'         => 'parent',
            'display_name' => 'Roditelj',
            'description'  => 'Roditelj',
        ]);

        $this->command->info('Creating Student Role');
        $this->command->info('');
        DB::table('roles')->insert([
            'name'         => 'student',
            'display_name' => 'Učenik - polaznik',
            'description'  => 'Učenik ili polaznik',
        ]);

        $this->command->info('Creating Superadmin Acc');
        $this->command->info('email: admin@victorious.co.rs');
        $this->command->info('pwd: victorious');

        DB::table('users')->insert([
            'firstname'  => 'Super',
            'lastname'   => 'Admin',
            'email'      => 'admin@victorious.co.rs',
            'password'   => bcrypt('victorious'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1,
        ]);
    }
}
