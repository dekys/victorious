<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventStudentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'event_student';

    /**
     * Run the migrations.
     * @table event_student
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('students_id');
            $table->unsignedInteger('events_id');

            $table->index(["events_id"], 'fk_students_has_events_events1_idx');

            $table->index(["students_id"], 'fk_students_has_events_students1_idx');


            $table->foreign('students_id', 'fk_students_has_events_students1_idx')
                ->references('id')->on('students')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('events_id', 'fk_students_has_events_events1_idx')
                ->references('id')->on('events')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
