<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseStudentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'course_student';

    /**
     * Run the migrations.
     * @table course_student
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('student_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->float('price')->nullable();

            $table->index(["course_id"], 'fk_students_has_courses_courses1_idx');

            $table->index(["student_id"], 'fk_students_has_courses_students1_idx');


            $table->foreign('student_id', 'fk_students_has_courses_students1_idx')
                ->references('id')->on('students')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('course_id', 'fk_students_has_courses_courses1_idx')
                ->references('id')->on('courses')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
