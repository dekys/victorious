<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'parents';

    /**
     * Run the migrations.
     * @table parents
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ime_oca', 20)->nullable()->default(null);
            $table->string('prezime_oca', 30)->nullable()->default(null);
            $table->string('telefon_oca')->nullable()->default(null);
            $table->string('email_oca');
            $table->string('ime_majke', 20)->nullable()->default(null);
            $table->string('prezime_majke', 30)->nullable()->default(null);
            $table->string('telefon_majke')->nullable()->default(null);
            $table->string('email_majke');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
