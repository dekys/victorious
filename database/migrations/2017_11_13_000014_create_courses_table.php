<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'courses';

    /**
     * Run the migrations.
     * @table courses
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('teacher_id');
            $table->unsignedInteger('service_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->tinyInteger('mon')->nullable()->default(null);
            $table->tinyInteger('tue')->nullable()->default(null);
            $table->tinyInteger('wed')->nullable()->default(null);
            $table->tinyInteger('thu')->nullable()->default(null);
            $table->tinyInteger('fri')->nullable()->default(null);
            $table->tinyInteger('sat')->nullable()->default(null);
            $table->tinyInteger('sun')->nullable()->default(null);
            $table->integer('payment_method')->nullable()->default(null);
            $table->float('amount')->nullable()->default(null);

            $table->index(["service_id"], 'fk_courses_services1_idx');

            $table->index(["teacher_id"], 'fk_courses_teachers1_idx');
            $table->nullableTimestamps();


            $table->foreign('teacher_id', 'fk_courses_teachers1_idx')
                ->references('id')->on('teachers')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('service_id', 'fk_courses_services1_idx')
                ->references('id')->on('services')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
