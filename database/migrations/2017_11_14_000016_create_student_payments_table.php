<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentPaymentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'student_payments';

    /**
     * Run the migrations.
     * @table student_payments
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->string('description', 200);
            $table->float('income')->nullable()->default(null);
            $table->float('outcome')->nullable()->default(null);
            $table->dateTime('date');
            $table->timestamps();

            $table->index(["student_id"], 'fk_student_payments_students1_idx');


            $table->foreign('student_id', 'fk_student_payments_students1_idx')
                ->references('id')->on('students')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
