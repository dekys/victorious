<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'students';

    /**
     * Run the migrations.
     * @table students
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('parent_id');
            $table->string('ime', 20)->nullable()->default(null);
            $table->string('prezime', 30)->nullable()->default(null);
            $table->date('rodjen')->nullable()->default(null);
            $table->string('adresa', 100)->nullable()->default(null);
            $table->string('telefon')->nullable()->default(null);
            $table->string('email');
            $table->unsignedInteger('razred')->nullable()->default(null);
            $table->string('skola', 100)->nullable()->default(null);
            $table->longText('napomena')->nullable()->default(null);
            $table->char('jmbg', 13)->nullable()->default(null);
            $table->string('brlk', 20)->nullable()->default(null);
            $table->string('pu', 50)->nullable()->default(null);
            $table->integer('person')->nullable()->default(null);

            $table->index(["parent_id"], 'fk_students_parents1_idx');

            $table->unique(["email"], 'students_email_unique');
            $table->nullableTimestamps();

            $table->foreign('parent_id', 'fk_students_parents1_idx')
                ->references('id')->on('parents')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_schema_table);
    }
}
