<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'subjects';

    /**
     * Run the migrations.
     * @table subjects
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->string('name', 50)->nullable()->default(null);
            $table->string('teacher', 50)->nullable()->default(null);
            $table->integer('mark')->nullable()->default(null);
            $table->text('comment')->nullable()->default(null);

            $table->index(["student_id"], 'fk_subjects_students1_idx');
            $table->nullableTimestamps();


            $table->foreign('student_id', 'fk_subjects_students1_idx')
                ->references('id')->on('students')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
