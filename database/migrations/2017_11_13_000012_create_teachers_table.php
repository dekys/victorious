<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'teachers';

    /**
     * Run the migrations.
     * @table teachers
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->string('ime');
            $table->string('prezime');
            $table->date('rodjen');
            $table->string('adresa');
            $table->string('telefon');
            $table->string('email');
            $table->string('jmbg');
            $table->string('brlk');
            $table->string('pu');
            $table->text('biografija');
            $table->string('vocation', 50)->nullable()->default(null);
            $table->string('school', 100)->nullable()->default(null);
            $table->string('address', 100)->nullable()->default(null);
            $table->string('percent', 5)->nullable()->default(null);

            $table->index(["category_id"], 'fk_teachers_categories1_idx');
            $table->nullableTimestamps();


            $table->foreign('category_id', 'fk_teachers_categories1_idx')
                ->references('id')->on('categories')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
