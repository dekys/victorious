<?php

use App\Location;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {

    if (Auth::check()) {
        $location = Location::first();

        if (isset($location)) {
            return redirect('dashboard/' . $location->id);
        } else {
            return redirect('location/create');
        }
    }
    return view('auth.login');
});

Route::get('/home', function () {
    return redirect('/');
});



/*
Iz rute je izbacen da ne bi prikazivao registraciju korisnika
\vendor\laravel\framework\src\Illuminate\Routing\Router.php
line: 306, 307
 */


Route::get('dashboard/{location_id}', 'HomeController@index');
Route::resource('profile', 'ProfileController');
Route::post('/profile/{id}/roles', 'ProfileController@UpdateRoles');
Route::patch('profile/{profile}/password', 'ProfileController@update_password');
Route::post('profile/{profile}', 'ProfileController@update_avatar');

Auth::routes();

Route::resource('message', 'MessageController');
// Route::get('events', 'EventController@index');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

// // Rute za manipulaciju sa roditeljima
// Route::resource('parent', 'ParentController');

//ADMINISTRATOR prava pristupa
Route::group(['middleware' => ['auth', 'role:admin']], function () {

// Route::get('search', 'AdministratorStudentController@search');

    // Rute za manipulaciju sa polaznicima
    Route::resource('administrator/student', 'AdministratorStudentController');
    Route::delete('administrator/student/subject/{id}', 'AdministratorStudentController@subject_destroy')
        ->name('student.subject_destroy');
    Route::put('administrator/student/subject/{id}', 'AdministratorStudentController@subject_update')
        ->name('student.subject_update');
    Route::post('administrator/student/subject', 'AdministratorStudentController@subject_store')
        ->name('student.subject_store');
    Route::get('administrator/student/{id}/pdf/prijava', 'AdministratorStudentController@PdfPrijava')
        ->name('student.prijava');

    // Rute za manipulaciju sa nastavnicima
    Route::resource('administrator/teacher', 'AdministratorTeacherController');
    Route::get('administrator/teacher/pdf/spisak', 'AdministratorTeacherController@PdfSpisakPredavaca')
        ->name('teacher.spisak');
    Route::get('administrator/teacher/{id}/pdf/prijava', 'AdministratorTeacherController@PdfPrijava')
        ->name('teacher.prijava');
    Route::get('administrator/teacher/pdf/ugovor', 'AdministratorTeacherController@PdfUgovor')
        ->name('teacher.ugovor');

    // Rute za manipulaciju sa uslugama
    Route::resource('administrator/service', 'AdministratorServiceController');

    // Rute za manipulaciju sa kategorijama usluga
    Route::resource('administrator/category', 'AdministratorCategoryController');

    // Rute za manipulaciju sa kursevi
    Route::resource('administrator/course', 'AdministratorCourseController');
    Route::get('/findServices', 'AdministratorCourseController@findServices');
    Route::post('administrator/course/findStudent', 'AdministratorCourseController@findStudent')
        ->name('course.findStudent');
    Route::get('administrator/course/addStudent/{id}/{course_id}', 'AdministratorCourseController@addStudent')
        ->name('course.addStudent');
    Route::get('administrator/course/deleteStudent/{id}/{course_id}', 'AdministratorCourseController@deleteStudent')
        ->name('course.deleteStudent');
    Route::post('administrator/course/changePrice', 'AdministratorCourseController@changePrice')
        ->name('course.changePrice');     

    Route::resource('admin', 'AdminController');

    Route::resource('event', 'EventController');
    Route::post('event/attend', 'EventController@attend')
        ->name('event.attend');

    Route::resource('location', 'LocationController');

});
