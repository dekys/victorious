@extends('layouts.app')

@section('htmlheader_title')
    Unos novog predavača
@endsection

@section('contentheader_title')
    Unos novog predavača
@endsection

@section('header-extra')
    <!-- parsley CSS -->
    {!! Html::style('/css/parsley.css') !!}
    {!! Html::style('/plugins/datepicker/datepicker3.css') !!}
@endsection

@section('main-content')
    {!! Form::open(['route' => 'teacher.store', 'class' => 'form-signin', 'data-parsley-validate' ]) !!}
        @include('administrators.teachers._form')     
    {!! Form::close() !!}   
@endsection

@section('scripts-extra')

    <script type="text/javascript">
        window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<span class="error-text"></span>',
            classHandler: function (el) {
                return el.$element.closest('input');
            },
            successClass: 'valid',
            errorClass: 'invalid'
        };
    </script>

     <script type="text/javascript">
            $(function () {
        $('#birthday').datepicker({
             format: "dd.mm.yyyy",
             language: "rs-latin"
        });        
          });
    </script>

    <script type="text/javascript">
         ckeditor.replace( 'messageArea',
         {
          customConfig : 'config.js',
          toolbar : 'simple'
          })
    </script> 
    
    <!-- parsley JS-->
    {!! Html::script('/js/parsley.min.js') !!}
    {!! Html::script('/plugins/datepicker/bootstrap-datepicker.js') !!}
    {!! Html::script('/plugins/ckeditor/ckeditor.js') !!}   
   
@endsection
