<!DOCTYPE html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<head>	
		<title>Ugovor predavača {{ $teachers->ime }} {{ $teachers->prezime }} </title>

		<style>
			@page { 
				margin-top: 2cm; 
				margin-left: 2cm; 
				margin-right: : 2cm; 
				margin-bottom: : 2cm; 
			}
			body { font-family: DejaVu Sans; font-size: 12px;}
			table {border-collapse: collapse}
			td {
				height: 30px}
		</style>

	</head>
	
    <body>

		<table style="width:660px">
			<tbody>
				<tr>
				    <td>Na osnovu člana 202. Zakona o radu («Službeni glasnik Republike Srbije», broj 24/05 i 61/05), direktor Edukativnog kutka "VICTOURIOUS", sa sedištem u Pećka 29, Kruševac,  (dalje: poslodavac) i {{ $teachers->ime.' '.$teachers->prezime }} sa prebivalištem na adresi: {{ $teachers->adresa }}, sa jedinstvenim matičnim brojem {{$teachers->jmbg}} i brojem lične karte {{$teachers->brlk}} izdate od PU {{ $teachers->pu }} (dalje: zaposleni), dana {{ $date }} godine zaključuju</td>
			    </tr>

			    <tr> 
			    	<td style="text-align:center"> <strong>UGOVOR O DOPUNSKOM RADU </strong></td> 
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 1.</strong></td>
			    </tr>
			    <tr>
			    	<td>Ugovorene strane konstatuju da je zaposleni u radnom odnosu kod poslodavca {{ $teachers->school }}, {{ $teachers->address }} kao {{ strtolower($teachers->vocation) }} sa punim radnim vremenom. <br>
			    	Ugovorene strane konstatuju da se po ovom ugovoru zaposleni angažuje za obavljanje određenih poslova van radnog odnosa.
			    	</td>
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 2.</strong></td>
			    </tr>
			    <tr>
			    	<td>Zaposleni zaključuje ovaj ugovor radi obavljanja poslova pomoć pri učenju i izradi domaćih zadataka. <br> Zaposleni će obavljati poslove u Edukativnom kutku Victorious, Pećka 29, Kruševac kao mestu rada.
				</td>
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 3.</strong></td>
			    </tr>
			    <tr>
			    	<td>Ovaj ugovor se zaključuje za period počev od {{ $teachers->date_from }} godine, pa do zaključno sa {{ $teachers->date_to }} godine, odnosno u trajanju od 1 mesec.</td>
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 4.</strong></td>
			    </tr>
			    <tr>
			    	<td>Zaposleni je dužan da poslove po ovom ugovoru obavlja najviše do jedne trećine punog radnog vremena, odnosno  sa radnim vremenom u trajanju od 10 sati nedeljno.</td>
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 5.</strong></td>
			    </tr>
			    <tr>
			    	<td>Zarada zaposlenog po ovom ugovoru utvrđena je srazmerno dužini trajanja radnog vremena, a u skladu sa zakonom i opštim aktom poslodavca, s tim što na dan zaključenja ugovora iznosi {{ number_format($teachers->cena,2,',', '.') }} dinara neto. <br>	
			    	Zarada se tokom trajanja ovog ugovora može menjati u skladu sa opštim aktom kojim se uređuju pitanja zarade zaposlenih kod poslodavca.  
					</td>
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 6.</strong></td>
			    </tr>
			    <tr>
			    	<td>Zaposlenom po ovom ugovoru pripadaju druga prava i obaveze po osnovu rada, u skladu sa zakonom.</td>
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 7.</strong></td>
			    </tr>
			    <tr>
			    	<td>Za sve što nije predviđeno ovim ugovorom a tiče se predmeta ugovora, primenjivaće se odredbe Zakona o radu i opšteg akta poslodavca.</td>
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 8.</strong></td>
			    </tr>
			    <tr>
			    	<td>U slučaju spora nadležan je sud u Kruševcu.</td>
			    </tr>
			    <tr>
			    	<td style="text-align:center"><strong>Član 9.</strong></td>
			    </tr>
			    <tr>
			    	<td>Ugovor je sačinjen u dva istovetna primerka, od kojih jedan primerak pripada zaposlenom, a drugi poslodavcu. </td>
			    </tr>

			</tbody>
		</table>


		<br>
		<br>
	  	<br>
		<br>

		<table>
			<tr>
				<td style="width:300px; text-align:center">POSLODAVAC</td>
				<td style="width:60px"> </td>
				<td style="width:300px; text-align:center">ZAPOSLENI</td>
			</tr>
			<tr>
				<td style="width:300px; text-align:center">________________________________</td>
				<td style="width:60px"> </td>
				<td style="width:300px; text-align:center">________________________________</td>
			</tr>
		</table>

    </body>
</html>