<!DOCTYPE html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<head>	
		<title>Prijava predavača {{ $teachers->ime }} {{ $teachers->prezime }} </title>

		<style>
			@page { 
				margin-top: 2cm; 
				margin-left: 2cm; 
				margin-right: : 2cm; 
				margin-bottom: : 2cm; 
			}
			body { font-family: DejaVu Sans; font-size: 12px;}
			table {border-collapse: collapse}
			th, td {
				/*border: 1px solid black; */
				height: 40px}
		</style>
		<div style="width:660px" align="center">
			<img src="{{ public_path() . '/img/logo.png' }}" style="width:300px" align="center">
		</div>

		<br>

		<h1>Prijava predavača </h1>

	</head>
	
    <body>

		<table>		
			<tbody>
			    <tr>
			        <td> Ime i prezime: </td> 
			        <td> <strong> {{ strtoupper($teachers->ime) }} {{ strtoupper($teachers->prezime) }} </strong> </td>
			    </tr>
			    <tr>
				    <td>Adresa stanovanja:</td>
				    <td><strong> {{ $teachers->adresa }} </strong></td>
			    </tr>
				<tr>
				    <td>JMBG:</td>
				    <td><strong> {{ $teachers->jmbg }}  </strong></td>
			    </tr>
			    <tr>
				    <td>Lična karta:</td>
				    <td><strong> {{ $teachers->brlk }} izdata od  {{ $teachers->pu }} </strong></td>
			    </tr>
			    <tr>
				    <td>Broj telefona:</td>
				    <td><strong> {{ $teachers->telefon }}  </strong></td>
			    </tr>		    
			    <tr>
				    <td>E-mail adresa:</td>
				    <td><strong> {{ $teachers->email }}  </strong></td>
			    </tr>
			    
			</tbody>
		</table>

		<table style="width:660px">
			<tbody>
				<tr>
				    <td valign="bottom">Radno iskustvo (biografija):</td>
			    </tr>
			    	<tr>
				    <td valign="top">{!! $teachers->biografija !!} </td>
			    </tr>
			</tbody>
		</table>

		<br>
		<br>
	  	<br>
		<br>

		<table>
			<tr>
				<td style="width:300px">U Kruševcu, dana {{ $date }}</td>
				<td style="width:60px"> </td>
				<td align="center" style="width:300px">Profesor</td>
			</tr>
			<tr>
				<td style="width:300px"> </td>
				<td style="width:60px"> </td>
				<td align="center" style="width:300px">	________________________________</td>
			</tr>
		</table>

    </body>
</html>