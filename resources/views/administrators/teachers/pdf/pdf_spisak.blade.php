<!DOCTYPE html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<head>	
		<title>Spisak predavača</title>
		<style>
			@page { 
				margin-top: 1cm; 
				margin-left: 1cm; 
				margin-right: : 1cm; 
				margin-bottom: : 1cm; 
			}
			body { font-family: DejaVu Sans; font-size: 12px;}
			table {border-collapse: collapse;}
			table, th, td {border: 1px solid black;}
		</style>

			<img src="{{ public_path() . '/img/logo.png' }}" style="width:200px">
			<br>

		<h1>Spisak svih predavača </h1>
		<h3>Ukupno broj predavača: {{ $sum_teachers }} </h3>

	</head>
	
    <body>
		<table>
		    <thead>
		        <tr>
		        	<th>R.br.</th>
					<th>Ime i prezime</th>
					<th>JMBG</th> 
					<th>Telefon</th>
					<th>E-mail</th>
					<th>Adresa</th>
					<th>Br.lk.</th>
				</tr> 
		    </thead>

		    <tbody>
				<?php $i = 0 ?>
		        @foreach ($teachers as $teacher)
		        	<?php $i++ ?>
		            <tr>
		            	<td>{{$i}}.</td>
						<td>{{$teacher->ime}} {{$teacher->prezime}}</td>
						<td>{{$teacher->jmbg}}</td>
						<td>{{$teacher->telefon}}</td>
						<td>{{$teacher->email}}</td>
						<td>{{$teacher->adresa}}</td>
						<td>{{$teacher->brlk}} {{$teacher->pu}}</td>
		            </tr>
		        @endforeach
		    </tbody>  
		</table>
	</body>
</html>