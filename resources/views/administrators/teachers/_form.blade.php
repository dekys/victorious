    <!-- Unos podataka o uceniku -->

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            {{-- <div class="box-header with-border"> --}}
            {{-- </div> --}}
            <div class="box-body">
                <h4>Osnovni podaci predavača</h4>

                    <div class="row">
                        <div class="col-sm-4">
                            {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                            {{ Form::text('ime', null, 
                                [
                                'class' => 'form-control form-group',
                                'placeholder'                   =>'Ime predavača',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'',  
                                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                'data-parsley-minlength'        => '2',
                                'data-parsley-maxlength'        => '32'
                                ])
                            }} 
                        </div>

                        <div class="col-sm-4">
                            {{-- {!! Form::label('prezime', 'Prezime * :') !!} --}}
                            {{ Form::text('prezime', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   =>'Prezime predavača',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'', 
                                'maxlength'                     =>"50"
                                ])
                            }}   
                        </div>
            
                        <div class="col-sm-4">
                            {{-- {!! Form::label('rodjen', 'Datum rođenja * :') !!} --}}
                            {{ Form::text('rodjen',  $datum,  
                                [
                                'class'                         => 'form-control form-group', 
                                'id'                            =>'birthday', 
                                'placeholder'                   =>'Datum rođenja',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'', 
                                'maxlength'                     =>"20",
                                ])
                            }} 
                        </div> 
                    </div>


    
                    <div class="row">
                        <div class="col-sm-4">
                            {{-- {{ Form::label('adresa', 'Adresa prebivališta:') }} --}}
                            {{ Form::text('adresa', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   =>'Adresa prebivališta',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'', 
                                'maxlength'                     =>"50"
                                ])
                            }}
                        </div>

                        <div class="col-sm-4">
                            {{-- {{ Form::label('telefon', 'Kontakt telefon:') }} --}}
                            {{ Form::text('telefon', null, 
                                [
                                'class'                         =>'form-control form-group', 
                                'placeholder'                   =>'Broj telefona',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'',
                                'maxlength'                     =>"50"
                                ]) 
                            }}
                        </div>  

                        <div class="col-sm-4">
                            {{-- {{ Form::label('email', 'Email adresa:') }} --}}
                            {{ Form::text('email', null, 
                                [
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   =>'Email adresa',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'data-type-email-message'       =>'Unesite validnu email adresu',
                                'required'                      =>'',
                                'data-parsley-type'             => 'email'
                                ]) 
                            }}
                        </div>  
                    </div>


   
                    <div class="row">
                        <div class="col-sm-4">
                            {{-- {{ Form::label('jmbg', 'JMBG:') }} --}}
                            {{ Form::text('jmbg', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   => 'Jedinstveni matični broj građana',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '', 
                                'maxlength'                     => '13',
                                'minlength'                     => '13'
                                ])
                            }}
                        </div>

                        <div class="col-sm-4">
                            {{-- {{ Form::label('brlk', 'Broj lične karte:') }} --}}
                            {{ Form::text('brlk', null, 
                                [
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   => 'Broj lične karte',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '',
                                'maxlength'                     => "50"
                                ]) 
                            }}
                        </div>  

                        <div class="col-sm-4">
                            {{-- {{ Form::label('pu', 'Policijska uprava:') }} --}}
                            {{ Form::text('pu', null, 
                                [
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   => 'Policijska uprava koja je izdala ličnu kartu',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '',
                                ]) 
                            }}
                        </div>  
                    </div>

                    <h4>Podatak o angažovanju</h4> 
                    <ul>
                        <li>u slučaju da traženi podaci o angažovanju <strong>ne budu uneti</strong> štampaće se Ugovor delu</li>
                        <li>u slučaju da traženi podaci o angažovanju <strong>budu uneti</strong> štampaće se Ugovor dopunskom</li>
                    </ul>
                    


                    <div class="row">
                        <div class="col-sm-3">
                            {{-- {{ Form::label('vocation', 'Zanimanje:') }} --}}
                            {{ Form::text('vocation', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   => 'Naziv zanimanja',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                ])
                            }}
                        </div>

                        <div class="col-sm-3">
                            {{-- {{ Form::label('school', 'Ustanova:') }} --}}
                            {{ Form::text('school', null, 
                                [
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   => 'Ime ustanove',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 

                                ]) 
                            }}
                        </div>  

                        <div class="col-sm-3">
                            {{-- {{ Form::label('address', 'Adresa:') }} --}}
                            {{ Form::text('address', null, 
                                [
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   => 'Adresa ustanove',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 

                                ]) 
                            }}
                        </div>  

                        <div class="col-sm-3">
                            {{-- {{ Form::label('percent', 'Procenat:') }} --}}
                            {{ Form::text('percent', null, 
                                [
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   => 'Procenat angažovanja',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                //'required'                      => '',
                                ]) 
                            }}
                        </div>  
                    </div>
            

                    <div class="row">
                        <div class="col-sm-3">
                            {{-- {!! Form::label('Kategorija', 'Kategoriju usluga:') !!} --}}
                            {{Form::select('category_id', 
                                $services, 
                                null, 
                                [
                                'class'=> 'form-control form-group',
                                'placeholder' => 'Odaberite internu kategoriju usluge...', 
                                ])
                            }} 
                        </div> 
                    </div>
     

                <h4>Biografija predavača</h4>

                    <div class="row">
                        <div class="col-sm-12">

                            {{-- {!! Form::label('biografija', 'Biografija predavača:') !!} --}}
                            {{ Form::textarea('biografija',  null,  
                                [
                                'id'                            => 'messageArea',
                                'resize'                        => 'none',
                                'rows'                          => '7',
                                'class'                         => 'form-control ckeditor', 
                                 'placeholder'                  => 'Kratka biografija predavača',
                                ])
                            }} 
                        </div> 
                    </div>

                @if (!Request::is('*/edit'))
                    <div class="row col-md-4">
                        {{Form::checkbox('create_user', '1', false)}} {{ 'Kreiraj korisnika' }}
                    </div>
                @else
                    <br>               
                @endif         

      
                  

                <!-- Dugmici -->
                <div class="row">
                    <div class="col-md-12 col-md-offset-8">
                        <div class="row">
                            <div class="col-md-2">
                                {{ Form::button('Poništi', array('class' => 'btn btn-primary  btn-block', 'type'=>'reset')) }} 
                            </div>
                            <div class="col-md-2">
                                {{ Form::button('Prihvati',  array('class' => 'btn btn-primary btn-block', 'type'=>'submit')) }} 
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- end of .row dugmici-->
            </div> 
        </div> 
    </div>
</div> <!-- end of .row -->

   
