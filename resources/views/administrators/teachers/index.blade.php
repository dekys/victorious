@extends('layouts.app')

@section('htmlheader_title')
    Predavači

@endsection

@section('contentheader_title')
    Predavači
@endsection

@section('header-extra')
    {!! Html::style('/css/parsley.css') !!}
    {!! Html::style('/plugins/datepicker/datepicker3.css') !!}
@endsection

@section('main-content')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">

                @if ($q!==null)
                    Prikaz rezultata pretrage predavača za "{{$q}}"
                @else
                    Prikaz svih predavača
                @endif

            </h3>

        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped ">

                        <thead>
                            <th>R.br.</th>
                            <th>Ime i prezime</th>
                            <th>Datum rođenja</th>
                            <th>Telefon</th>
                            <th>Adresa</th>
                            <th>Predmet</th>
                            <th>Balans</th>
                            <th> </th>
                        </thead>

                        <tbody>
                            @php
                                $i = ($teachers->currentPage()-1)*$teachers->perPage();
                            @endphp
                         
                            @foreach ($teachers as $teacher)
                                <tr>
                                    <td> {{ ++$i }}.</td>
                                    <td> <a href="{{ route('teacher.show', $teacher->id) }}">{{ $teacher->ime }} {{ $teacher->prezime }}</a>  </td>
                                    <td> {{ date('d.m.Y.', strtotime( $teacher->rodjen)) }} </td>
                                    <td> {{ $teacher->telefon }} </td>
                                    <td> {{ $teacher->adresa }} </td>  
                                    <td> {{ $teacher->category->naziv or ''}} </td>
                                    <td> {{number_format(rand(5, 15)*1509.325/1.2, 2, ',', '.')}}</td>
                                    <td class="td" align="right" style="width:200px">
                                        
                                        <a href="{{ route('teacher.edit', $teacher->id)}}" class="btn" title="Izmeni podatke"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="{{ route('teacher.prijava', $teacher->id) }}" class="btn" title="Štampanje prijave"><i class="fa fa-file-text-o"></i></a>
                                        <a href="" id="ugid" data-toggle="modal" data-target="#preUgovor" data-teacher_id="{{ $teacher->id }}" class="btn" title="Štampanje ugovora"><i class="fa fa-file-text-o"></i></a>
                                        <a href="" data-teacher_id="{{ $teacher->id }}" data-teacher_name="{{ $teacher->name }}" data-toggle="modal" data-target="#confirmDelete" class="btn" title="Brisanje podataka"><i class="fa fa-trash-o"></i></a>

                                    </td>
                                </tr>
                            @endforeach

                        </tbody>  

                        </table>

                        <div class="text-center">
                            {!! $teachers->appends(Request::only('q'))->links(); !!}                
                            {{-- $posts->links() --}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

    <!-- Modal za brisanje -->
    <div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Potvrda brisanja predavača</h4>
              </div>
              <div class="modal-body">
                <p>Da li ste sigurni da želite obrisati odabranu predavača?</p>
              </div>
              <div class="modal-footer">
                {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Izbriši</button>
                {!! Form::close() !!}
              </div>
            </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 

    <!-- Modal za dodavanje elemenata ugovora -->
    <div class="modal fade" id="preUgovor" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Potrebni elementi ugovora</h4>
              </div>
              <div class="modal-body">

                    {{-- <form id="forma"> --}}
                {!! Form::open(['method'=>'GET','route' => ['teacher.ugovor'], 'onSubmit'=>"CloseModal();"]) !!}
                    {{ Form::hidden('teacher_id', 'teacher_id', array('id' => 'teacher_id' )) }}
  
                        <div class="row">

                            <div class="col-sm-4">
                                {{-- {!! Form::label('rodjen', 'Datum rođenja * :') !!} --}}
                                {{ Form::text('date_from',  '',  
                                    [
                                    'class'                         => 'form-control form-group', 
                                    'id'                            =>'date_from', 
                                    'placeholder'                   =>'Datum početka ugovora',
                                    'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                    'required'                      =>'', 
                                    //'maxlength'                     =>"20",
                                    ])
                                }} 
                            </div> 

                            <div class="col-sm-4">
                                {{-- {!! Form::label('rodjen', 'Datum rođenja * :') !!} --}}
                                {{ Form::text('date_to',  null,  
                                    [
                                    'class'                         => 'form-control form-group', 
                                    'id'                            =>'date_to', 
                                    'placeholder'                   =>'Datum kraja ugovora',
                                    'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                    'required'                      =>'', 
                                    //'maxlength'                     =>"20",
                                    ])
                                }} 
                            </div>

                            <div class="col-sm-4">
                                {{-- {!! Form::label('rodjen', 'Datum rođenja * :') !!} --}}
                                {{ Form::text('cena',  null,  
                                    [
                                    'class'                         => 'number form-control form-group', 
                                    'id'                            =>'cena', 
                                    'placeholder'                   =>'Ugovorena cena',
                                    'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                    'required'                      =>'', 
                                    //'maxlength'                     =>"20",
                                    ])
                                }} 
                            </div>

                        </div>
              </div>
              <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-success">Prihvati</button>
                {!! Form::close() !!}
              </div>
            </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 

@endsection

@section('scripts-extra')

    <script type="text/javascript">
        $(function () {
            $('#date_from, #date_to').datepicker({
                 format: "dd.mm.yyyy.",
                 language: "rs-latin",
                 autoclose: true
            });        
          });

        $('#confirmDelete').on('show.bs.modal', function(e) {
            var teacherId = $(e.relatedTarget).data('teacher_id');
            $("#delForm").attr('action', 'teacher/'+teacherId);
        });

        $('#preUgovor').on('shown.bs.modal', function(e) {
            var teacherId = $(e.relatedTarget).data('teacher_id');
            var modal = $(this);
            modal.find('#teacher_id').val(teacherId);
            modal.find('#date_from').val('');
            modal.find('#date_to').val('');
            modal.find('#cena').val('');
        });

 
        $(document).ready(function() {
            $('#example').DataTable({
                "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
                },
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "searching": false              
            });
        } );

    </script>


    {!! Html::script('/plugins/datepicker/bootstrap-datepicker.js') !!}
    {!! Html::script('/js/parsley.min.js') !!}


@endsection
