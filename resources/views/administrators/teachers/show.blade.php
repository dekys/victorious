@extends('layouts.app')

@section('htmlheader_title')
    Prikaz informacija o predavaču
@endsection

@section('contentheader_title')
    Prikaz informacija o predavaču
@endsection

@section('header-extra')

@endsection

@section('main-content')
    <!-- Unos podataka o uceniku -->

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <h4>Osnovni podaci predavača</h4>
                <div class="col-md-12">
                    <table class="table table-striped">     
                        <tbody>
                            <tr>
                                <td style="width:200px"> Ime i prezime: </td> 
                                <td> <strong> {{ $teacher->ime }} {{ $teacher->prezime }} </strong> </td>
                            </tr>
                            <tr>
                                <td>Adresa stanovanja:</td>
                                <td><strong> {{ $teacher->adresa }} </strong></td>
                            </tr>
                            <tr>
                                <td>JMBG:</td>
                                <td><strong> {{ $teacher->jmbg }}  </strong></td>
                            </tr>
                            <tr>
                                <td>Lična karta:</td>
                                <td><strong> {{ $teacher->brlk }} izdata od  {{ $teacher->pu }} </strong></td>
                            </tr>
                            <tr>
                                <td>Broj telefona:</td>
                                <td><strong> {{ $teacher->telefon }}  </strong></td>
                            </tr>
                              <tr>
                                <td>E-mail adresa:</td>
                                <td><strong> {{ $teacher->email }}  </strong></td>
                            </tr> 
                            <tr>
                                <td>Kategorija usluge:</td>
                                <td><strong> {{ $teacher->category->naziv or '' }}  </strong></td>
                            </tr> 
                            <tr>
                                <td valign="top">Biografija:</td>
                                <td>
                                    {!! $teacher->biografija !!} 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> 

                <h4>Podaci o radnom angažovanju</h4>   
                <div class="col-md-12">
                    <table class="table table-striped">     
                        <tbody>
                            <tr>
                                <td style="width:200px"> Zanimanje: </td> 
                                <td> <strong> {{ $teacher->vocation }}</strong> </td>
                            </tr>
                            <tr>
                                <td>Ustanova:</td>
                                <td><strong> {{ $teacher->school }} </strong></td>
                            </tr>
                            <tr>
                                <td>Adresa ustanove:</td>
                                <td><strong> {{ $teacher->address }}  </strong></td>
                            </tr>
                            <tr>
                                <td>Procenat:</td>
                                <td><strong> {{ $teacher->percent }} </strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> 
        </div> 
    </div>

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <h4>Transakcije</h4>
                <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">     
                         <thead>
                          <tr>
                              <th style="width:200px">Opis</th>
                              <th>Prihod</th>
                              <th>Rashod</th>
                              <th>Datum</th>
                              <th></th>
                          </tr>
                          </thead>
                        <tbody>
                           @foreach ($teacher->payments as $payment)
                               <tr>
                                   <td>{{ $payment->description }}</td>
                                   <td>{{ $payment->income }}</td>
                                   <td>{{ $payment->outcome }}</td>
                                   <td>{{ $payment->date }}</td>

                                      <td class="td" align="right" style="width:200px">
                                          <a href="" class="btn-sm btn" title="Izbrisi polaznika"><i class="fa fa-eye"></i></a> 

                                          <a href="" class="btn btn-sm" title="Izbrisi polaznika"><i class="fa fa-trash"></i></a> 

                                      </td>
                               </tr>
                           @endforeach
                           <tr>
                               <td>Total</td>
                               <td>6559465</td>
                               <td>654654</td>
                               <td>27/15/</td>
                               <td></td>
                           </tr>
                        </tbody>
                    </table>
                </div> 
            </div> 
            </div>
        </div> 
    </div>
</div> <!-- end of .row -->


@endsection

@section('scripts-extra')

@endsection