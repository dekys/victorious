@extends('layouts.app')

@section('htmlheader_title')
    Termini
@endsection

@section('contentheader_title')
    Termini
@endsection

@section('header-extra')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css"/>
@endsection

@section('main-content')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Prikaz svih termina</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped">
                            <thead>
                                <th>R.br.</th>
                                <th>Usluga</th>
                                <th>Kategorija</th>
                                <th>Početak</th>
                                <th>Kraj</th>
                                <th>Vreme</th>
                                <th>Predavač</th>
                                <th>Lokacija</th>
                                <th>Pon.</th>
                                <th>Uto.</th>
                                <th>Sre.</th>
                                <th>Čet.</th>
                                <th>Pet.</th>
                                <th>Sub.</th>
                                <th>Ned.</th>
                                <th> </th>
                            </thead>

                            <tbody>

                                @php
                                    $i = ($courses->currentPage()-1)*$courses->perPage();
                                @endphp

                                @foreach ($courses as $course)
                                    <tr>
                                        <td> {{ ++$i }}.</td>
                                        <td> <a href="{{ route('service.edit', $course->service_id) }}"> {{ $course->service->naziv or '' }} </a></td>
                                        <td> {{ $course->service->category->naziv or '' }} </td>
                                        <td> {{ date('d.m.Y.', strtotime($course->start_date)) }} </td>
                                        <td> {{ date('d.m.Y.', strtotime($course->end_date)) }} </td>
                                        <td> {{ date('H:i', strtotime($course->start_time)) }} - {{date('H:i', strtotime($course->end_time))}} </td>
                                        <td> <a href="{{ route('teacher.edit', $course->teacher_id) }}"> {{ $course->teacher->ime }} {{$course->teacher->prezime }}</a></td>
                                        <td> <a href="{{ route('location.edit', $course->location_id) }}"> {{ $course->location->name }} </a></td>
                                        <td> {!! isset($course->mon) ? "<p align='center'><i class='fa fa-check'></i></p>" : ''!!} </td>
                                        <td> {!! isset($course->tue) ? "<p align='center'><i class='fa fa-check'></i></p>" : ''!!} </td>
                                        <td> {!! isset($course->wed) ? "<p align='center'><i class='fa fa-check'></i></p>" : ''!!} </td>
                                        <td> {!! isset($course->thu) ? "<p align='center'><i class='fa fa-check'></i></p>" : ''!!} </td>
                                        <td> {!! isset($course->fri) ? "<p align='center'><i class='fa fa-check'></i></p>" : ''!!} </td>
                                        <td> {!! isset($course->sat) ? "<p align='center'><i class='fa fa-check'></i></p>" : ''!!} </td>
                                        <td> {!! isset($course->sun) ? "<p align='center'><i class='fa fa-check'></i></p>" : ''!!} </td>

                                        <td class="td" align="right" style="width:150px">
                                            
                                            <a href="{{ route('course.show', $course->id) }}"  class="btn" title="Prikaz trmina"><i class="fa fa-eye"></i></a> 
                                            <a href="{{ route('course.edit', $course->id) }}"  class="btn " title="Izmeni termina"><i class="fa fa-pencil-square-o"></i></a> 
                                            <a class="btn" data-course_id="{{ $course->id }}" data-course_name="{{ $course->name }}"  data-toggle="modal" data-target="#confirmDelete" title="Izbriši termin"><i class="fa fa-trash-o"></i></a> 
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>  

                        </table>

                        <div class="text-center">
                            {!! $courses->links(); !!}                
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
<!--  -->
    <div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Potvrda brisanja termina</h4>
              </div>
              <div class="modal-body">
                <p>Da li ste sigurni da želite obrisati odabrani Termin?</p>
              </div>
              <div class="modal-footer">
                {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Izbriši</button>
                {!! Form::close() !!}
              </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 


@endsection

@section('scripts-extra')
    <script type="text/javascript">

        $('#confirmDelete').on('show.bs.modal', function(e) {
            var courseId = $(e.relatedTarget).data('course_id');
            $("#delForm").attr('action', 'course/'+courseId);
        });

        $(document).ready(function() {
        $('#example').DataTable({
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
            },
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false
        });
        } );    
        
    </script>
@endsection