@extends('layouts.app')

@section('htmlheader_title')
    Unos novog kursa
@endsection

@section('contentheader_title')
    Unos novog kursa
@endsection

@section('header-extra')
<!-- parsley CSS -->
{!! Html::style('/css/parsley.css') !!}
    {!! Html::style('/plugins/datepicker/datepicker3.css') !!}
    {{-- {!! Html::style('/plugins/timepicker/bootstrap-timepicker.css') !!} --}}
    {!! Html::style('//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css') !!}

@endsection

@section('main-content')
    {!! Form::model($course,['route' => ['course.update', $course->id], 'method' => 'PUT', 'data-parsley-validate' =>'' ]) !!}
        @include('administrators.courses._form')     
    {!! Form::close() !!}   
@endsection

@section('scripts-extra')
<script type="text/javascript">
    window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<span class="error-text"></span>',
            classHandler: function (el) {
                return el.$element.closest('input');
            },
            successClass: 'valid',
            errorClass: 'invalid'
        };

        $('.category_id').change(function(){
            var cat_id=$(this).val();
            var opS=" ";
            var opT=" ";

            $.ajax({
                type:'get',
                url:'{!!URL::to('findServices')!!}',
                data:{
                        'id':cat_id,
                        '_token': $('input[name=_token]').val()
                },

                success:function(data){
                    if (data.services.length > 0) {
                            for(var i=0;i<data.services.length;i++){
                            opS+='<option value="'+data.services[i].id+'">'+data.services[i].naziv+'</option>';
                            }
                    }else {
                            opS='<option value="'+'0'+'">'+'Nema podataka'+'</option>';
                        }
                    $('.service_id').html(" ");
                    $('.service_id').append(opS);
                    if (data.teachers.length > 0 && data.services.length > 0) {
                            for(var i=0;i<data.teachers.length;i++){
                            opT+='<option value="'+data.teachers[i].id+'">'+data.teachers[i].ime+ " " +data.teachers[i].prezime+'</option>';
                            }
                    }else {
                            opT='<option value="'+'0'+'">'+'Nema podataka'+'</option>';
                        }
                    $('.teacher_id').html(" ");
                    $('.teacher_id').append(opT);
                },
                error:function(){
                }
            });
        });
</script>
<script type="text/javascript">
    $(function () {
            $( "input[id$='date']" ).datepicker({
             format: "dd.mm.yyyy.",
             language: "rs-latin"
            });
          });
</script>
<script type="text/javascript">
    $(function () {        
            $( "input[id$='time']" ).timepicker({
                timeFormat: 'HH:mm',
                interval: 15,
                minTime: '7',
                maxTime: '10:00pm',
                // defaultTime: '7',
                // startTime: '7:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });  
          });
</script>
<!-- parsley JS-->
{!! Html::script('/js/parsley.min.js') !!}
    {!! Html::script('/js/rs.js') !!}
    {!! Html::script('/plugins/datepicker/bootstrap-datepicker.js') !!}
    {{-- {!! Html::script('/plugins/timepicker/bootstrap-timepicker.js') !!} --}}
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js') !!}
    {!! Html::script('/plugins/ckeditor/ckeditor.js') !!} 


@endsection
