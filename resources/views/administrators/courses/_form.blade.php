<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <h4 class="box-title">
                    Usluga
                </h4>
                <div class="row">
                    <div class="col-sm-3">
                        {{-- Selektovanje kategorija --}}
                            {{Form::select('category_id', 
                                $categories, 
                                isset($course->service->category_id) ? $course->service->category_id : null,
                                [
                                'class'             => 'category_id form-control form-group',
                                'placeholder'       => 'Odaberite kategoriju...', 
                                'required'          => '', 
                                ])
                            }}
                    </div>
                    <div class="col-sm-3">
                        {{-- selektovanje usluga --}}
                        {{Form::select('service_id', 
                            $services, 
                            null,
                            [
                            'class'             => 'service_id form-control form-group ',
                            'placeholder'       => 'Odaberite uslugu...',
                            'required'          => '', 
                            ])
                        }}
                    </div>
                </div>
                <h4 class="box-title">
                    Predavač
                </h4>
                <div class="row">
                    <div class="col-sm-3">
                        {{-- selektovanje nastavnika --}}
                        {{Form::select('teacher_id', 
                                $teachers, 
                                null,
                            [
                            'class'             => 'teacher_id form-control form-group ',
                            'placeholder'       => 'Odaberite predavača...', 
                            'required'          => '', 
                            ])
                        }}
                    </div>
                    <div class="col-sm-3">
                        {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                            {{Form::select('payment_method', 
                                $payment_method,
                                isset($course->payment_method) ? $course->payment_method : null,
                                [
                                'class'=> 'form-control form-group',
                                'placeholder' => 'Način plaćanja...',
                                'required' => '', 
                                ])
                            }}
                    </div>
                    <div class="col-sm-2">
                        {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                            {{Form::text('amount', null, 
                                [
                                'class'=> 'form-control form-group',
                                'placeholder' => 'iznos...',
                                'required' => '', 
                                ])
                            }}
                    </div>
                </div>
                <h4 class="box-title">
                    Mesto i vreme realizacije
                </h4>
                <div class="row">
                    <div class="col-sm-3">
                        {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                            {{Form::select('location_id', 
                                $locations, 
                                null, 
                                [
                                'class'=> 'form-control form-group',
                                'placeholder' => 'Odaberite lokaciju...',
                                'required' => '', 
                                ])
                            }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        {{-- {{ Form::label('adresa', 'Adresa prebivališta:') }} --}}
                            {{ Form::text('start_date', 
                                $start_date,
                                [
                                'id'                            => 'start_date',
                                'class'                         => 'form-control form-group',
                                'placeholder'                   => 'Od datuma',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '', 
                                ])
                            }}
                    </div>
                    <div class="col-sm-2">
                        {{-- {{ Form::label('email', 'Email adresa:') }} --}}
                            {{ Form::text('end_date', 
                                $end_date, 
                                [
                                'id'                            => 'end_date',
                                'placeholder'                   => 'Do datuma',
                                'class'                         => 'form-control form-group', 
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'',
                                ]) 
                            }}
                    </div>
                    <div class="col-sm-2">
                        {{-- {{ Form::label('telefon', 'Kontakt telefon:') }} --}}
                            {{ Form::text('start_time', null, 
                                [
                                'id'                            => 'start_time',
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   => 'Vreme od',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '',
                                ]) 
                            }}
                    </div>
                    <div class="col-sm-2">
                        {{-- {{ Form::label('email', 'Email adresa:') }} --}}
                            {{ Form::text('end_time', null, 
                                [
                                'id'                            => 'end_time',
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   =>'vreme do',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'',
                                ]) 
                            }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <h4 class="box-title">
                            Ponavljanja
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="col-md-1">
                            {{Form::checkbox('mon', '1', isset($course->mon) ? $course->mon : false)}} {{ 'P' }}
                        </div>
                        <div class="col-md-1">
                            {{Form::checkbox('tue', '1', isset($course->tue) ? $course->tue : false)}} {{ 'U' }}
                        </div>
                        <div class="col-md-1">
                            {{Form::checkbox('wed', '1', isset($course->wed) ? $course->wed : false)}} {{ 'S' }}
                        </div>
                        <div class="col-md-1">
                            {{Form::checkbox('thu', '1', isset($course->thu) ? $course->thu : false)}} {{ 'Č' }}
                        </div>
                        <div class="col-md-1">
                            {{Form::checkbox('fri', '1', isset($course->fri) ? $course->fri : false)}} {{ 'P' }}
                        </div>
                        <div class="col-md-1">
                            {{Form::checkbox('sat', '1', isset($course->sat) ? $course->sat : false)}} {{ 'S' }}
                        </div>
                        <div class="col-md-1">
                            {{Form::checkbox('sun', '1', isset($course->sun) ? $course->sun : false)}} {{ 'N' }}
                        </div>
                    </div>
                </div>
                <!-- Dugmici -->
                <div class="row">
                    <div class="col-md-12 col-md-offset-8">
                        <div class="row">
                            <div class="col-md-2">
                                {{ Form::button('Poništi', array('class' => 'btn btn-primary  btn-block', 'type'=>'reset')) }}
                            </div>
                            <div class="col-md-2">
                                {{ Form::button('Prihvati',  array('class' => 'btn btn-primary btn-block', 'type'=>'submit')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of .row dugmici-->
            </div>
        </div>
    </div>
</div>
<!-- end of .row -->
