@extends('layouts.app')

@section('htmlheader_title')
    Pretraga polaznika
@endsection

@section('contentheader_title')
    Pretraga polaznika
@endsection


@section('header-extra')

@endsection


@section('main-content')


 <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Prikaz svih polaznika</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped">
                            <thead>
                                {{-- <th>R.br.</th> --}}
                                <th>Ime i prezime</th>
                                <th>Razred</th>
                                <th>Predmet</th>
                                <th>Datum rođenja</th>
                                <th>Telefon</th>
                                <th>Adresa</th>
                                {{-- <th>Kurs</th> --}}
                                {{-- <th>Balans</th> --}}
                                <th> </th>
                            </thead>

                            <tbody>
                                @php
                                    // $i = ($students->currentPage()-1)*$students->perPage();
                                @endphp
                                
                                @foreach ($students as $student)

                                    <tr id='{{ $student->id }}'>
                                        {{-- <td>{{ ++$i }}.</td> --}}
                                        <td> <a href="{{ route('student.show', $student->id) }}" title="Prikaži detalje">{{ $student->ime }} {{ $student->prezime }}</a></td>
                                        <td> {{ $student->razred}} </td>  
                                        <td> 
                                            @foreach ($student->subjects as $subject)
                                                <ul>
                                                    <li> {{$subject->name}} </li>
                                                </ul>
                                            @endforeach
                                        </td>  

                                        <td> {{ date('d.m.Y.', strtotime( $student->rodjen)) }} </td>
                                        <td> {{ $student->telefon}} </td>
                                        <td> {{ $student->adresa}} </td>  
                                        {{-- <td> </td> --}}
                                        {{-- <td></td> --}}
                                        <td class="td" align="right" style="width:100px">
                                            <form action="/administrator/course/addStudent" method="POST">
                                                {{ csrf_field() }}
                                                <a href="{{ route('course.addStudent', [ $student->id, $course->id]) }}"  class="btn " title="Dodaj polaznika"><i class="fa fa-check"></i></a> 
                                            </form>
                                        </td>
                                    </tr>
                                    
                                @endforeach

                            </tbody>  

                        </table>

                        <div class="text-center">
                            {{-- {!! $students->links(); !!}                 --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

{{--     <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Rezultat pretrage polaznika</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped">
                            <thead>
                                <th>Ime i prezime</th>
                                <th>Datum rođenja</th>
                                <th>Telefon</th>
                                <th>Adresa</th>
                                {{-- <th>Kurs</th> --}}
{{--                                 <th> </th>
                            </thead>

                            <tbody>

                                
                                @foreach ($students as $student)

                                    <tr id='{{ $student->id }}'>
                                        <td> {{ $student->ime }} {{ $student->prezime }} </td>
                                        <td> {{ date('d.m.Y.', strtotime( $student->rodjen)) }} </td>
                                        <td> {{ $student->telefon}} </td>
                                        <td> {{ $student->adresa}} </td>  
                                        {{-- <td></td> --}}
{{--                                         <td class="td" align="right" style="width:200px">
                                            <form action="/administrator/course/addStudent" method="POST">
                                                {{ csrf_field() }}
                                                <a href="{{ route('course.addStudent', [ $student->id, $course->id]) }}"  class="btn " title="Dodaj polaznika"><i class="fa fa-check"></i></a> 
                                            </form>     
                                        </td>
                                    </tr>

                                @endforeach

                            </tbody>  
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div> --}} 


@endsection

@section('scripts-extra')

    <script type="text/javascript">

        $(document).ready(function() {
            $('#example').DataTable({
                "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
                },
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false                
            });
        } );
        
     </script>

@endsection
