@extends('layouts.app')

@section('htmlheader_title')
    Prikaz kursa
@endsection

@section('contentheader_title')
    Prikaz kursa
@endsection

@section('header-extra')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css"/>

@endsection

@section('main-content')

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h2 class="box-title">{{$course->service->category->naziv}} - {{$course->service->naziv}} (događaja: {{$countEvents}})</h2>
            </div>
            <div class="box-body">
                <div class="row col-md-6">
                    <h4><ins>Kurs</ins></h4>
                    <p>Kategorija: <STRONG> {{$course->service->category->naziv}} </STRONG></p>
                    <p>Usluga: <STRONG> {{$course->service->naziv}} </STRONG></p>
                    <p>Cena usluge: <STRONG> {{number_format($course->service->cena, 2, ',', '.')}} RSD</STRONG></p>
                    <br>
                </div> 

                <div class="row col-md-6">
                    <h4><ins>Predavač</ins></h4>
                    <p>Ime i prezime: <STRONG> {{$course->teacher->ime or ''}} {{$course->teacher->prezime or ''}} </STRONG></p>
                    <p>Kontakt telefon: <STRONG> {{$course->teacher->telefon or ''}} </STRONG></p>
                    <p>E-mail: <STRONG> {{$course->teacher->email or ''}} </STRONG></p>
                    <br>
                </div> 

                <div class="row col-md-6">
                    <h4><ins>Lokacija realizacije</ins></h4>
                    <p>Naziv lokacije: <STRONG> {{$course->location->name}} </STRONG></p>
                    <p>Adressa lokacije: <STRONG> {{$course->location->address}} </STRONG></p>
                    <p>Kapacitet lokacije: <STRONG> {{$course->location->capacity}} polaznika</STRONG></p>
                    <br>
                </div> 

                <div class="row col-md-6">
                    <h4><ins>Period realizacije</ins></h4>
                    <p>Početka realizacije: <STRONG> {{Carbon::parse($course->start_date)->format('d.m.Y.') }}  godine</STRONG></p>
                    <p>Kraj realizacije: <STRONG> {{Carbon::parse($course->end_date)->format('d.m.Y.') }}   godine</STRONG></p>
                    <p>U terminu od <STRONG> {{$course->start_time}} </STRONG> do <STRONG>{{$course->end_time}}</STRONG></p>
                </div> 
        
            <div class="row">
            <div class="col-md-12">
                <div class="box-header with-border">
                    <h2 class="box-title"><ins>Polaznici</ins></h2>
                    <form action="/administrator/course/findStudent" method="POST" role="search">
                        {{ csrf_field() }}
                        {{ Form::hidden('course_id', $course->id) }}
                         <button type="submit" class="pull-right btn btn-default btn-sm"><span class="glyphicon glyphicon-search"></span></button>
                    </form>            
                </div>
            </div>
            </div>
  

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped">
                            <thead>
                                <th>Ime i prezime</th>
                                <th>Razred</th>
                                <th>Predmet</th>
                                <th>Datum rođenja</th>
                                <th>Telefon</th>
                                <th>Adresa</th>
                                <th>Cena</th>
                                <th> </th>
                            </thead>

                            <tbody>
                                
                                @foreach ($course->students as $student)

                                    <tr id='{{ $student->id }}'>

                                        <td> <a href="{{ route('student.show', $student->id) }}" title="Prikaži detalje">{{ $student->ime }} {{ $student->prezime }}</a></td>
                                        <td> {{ $student->razred}} </td>  
                                        <td> 
                                            @foreach ($student->subjects as $subject)
                                                    {{$subject->name}} 
                                            @endforeach
                                        </td>  
                                        <td> {{ date('d.m.Y.', strtotime( $student->rodjen)) }} </td>
                                        <td> {{ $student->telefon }} </td>
                                        <td> {{ $student->adresa }} </td>  
                                        <td>
                                        <a href="#" 
                                          data-price="{{number_format($student->pivot->price, 2, ',', '.')}}" 
                                          data-student_id="{{ $student->id }}"  
                                          data-course_id="{{ $course->id }}"  
                                          data-toggle="modal" 
                                          data-target="#editPrice""> {{number_format($student->pivot->price, 2, ',', '.')}} RSD
                                        </a>
                                      </td>
                                      <td class="td" align="right" style="width:100px">
                                          <a href="{{ route('student.show', $student->id) }}" class="btn-sm btn" title="Izbrisi polaznika"><i class="fa fa-eye"></i></a> 

                                          <a href="{{ route('course.deleteStudent', [$student->id, $course->id]) }}" class="btn btn-sm" title="Izbrisi polaznika"><i class="fa fa-trash"></i></a> 
                                      </td>
                                    </tr>
                                    
                                @endforeach

                            </tbody>  

                        </table>

                    </div>
                </div>
            </div>


        </div>
 
        </div> 
    </div> 



    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Događaji</h3>
            </div>
            <div class="box-body">
                <div class="row col-md-12">
                    <div id="calendar" class="fc fc-unthemed fc-ltr"> </div>
                </div> 
            </div> 
        </div> 
    </div>    
</div> <!-- end of .row -->


    {{-- MODAL EDIT CENA --}}
    <div class="modal fade" id="editPrice" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Izmena cene</h4>
              </div>
              {!! Form::open(['route' => 'course.changePrice', 'class' => 'form-signin', 'data-parsley-validate' ]) !!}
                <div class="modal-body">
                  <p>Unesite ovi iznos koji plaća polaznik.</p>
                    <div class="row">
                      <div class="col-sm-4">
                  
                      {{Form::hidden('student_id', null, 
                            [
                            'class'=> 'form-control form-group',
                            'id'=> 'student_id'
                            ])
                        }}
                        {{Form::hidden('course_id', null, 
                            [
                            'class'=> 'form-control form-group',
                            'id'=> 'course_id'
                            ])
                        }}
                        {{Form::text('amount', null, 
                            [
                            'class'=> 'form-control form-group',
                            'id'=> 'amount',
                            'placeholder' => 'iznos...',
                            'required' => '', 
                            ])
                        }}
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                      <button type="submit" class="btn btn-primary">Prihvati</button>
                </div>
              {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 
   
@endsection

@section('scripts-extra')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="{{ asset('plugins/fullcalendar/locale/sr.js') }}" type="text/javascript"></script>
 <script>

        $(document).ready(function() {
           var $calendar = $('#calendar').fullCalendar(
            {   
                snapDuration: '00:15:00',
                editable: true,
                minTime: "07:00:00",
                maxTime: "22:00:00",
               // contentHeight: 800,
                header: {
                    left: 'today',
                    center: 'title',
                    // right: 'listMonth, month,agendaWeek prev,next'
                    right: 'prev,next'
                    },
                defaultView: 'listMonth',
                events: [

                        @foreach($course->events as $event)
                        {
                            id : '{{ $event->id }}',
                            title : '{{ $event->title }}  xxx',
                            start : '{{ $event->start }}',
                            end : '{{ $event->end }}',
                            color: '{{ $event->background_color }}'
                        },
                        @endforeach
                ],
            });
        });


        $('#editPrice').on('shown.bs.modal', function(e) {
            var price = $(e.relatedTarget).data('price');
            var course_id = $(e.relatedTarget).data('course_id');
            var student_id = $(e.relatedTarget).data('student_id');
            var modal = $(this);
            modal.find('#student_id').val(student_id);
            modal.find('#course_id').val(course_id);
            modal.find('#amount').val(price);

        });

    </script>
@endsection