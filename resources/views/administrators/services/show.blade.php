@extends('layouts.app')

@section('htmlheader_title')
    Prikaz informacija o polazniku
@endsection

@section('contentheader_title')
    Prikaz informacija o polazniku
@endsection

@section('header-extra')

@endsection

@section('main-content')
    <!-- Unos podataka o uceniku -->

<div class="row">
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Osnovni podaci polaznika</h3>
            </div>
            <div class="box-body">
                <div class="row col-md-12">
                    <p>Ime: <STRONG> {{ $students->ime }} </STRONG></p>
                    <p>Prezime: <STRONG> {{ $students->prezime }} </STRONG></p>
                    <p>Adressa: <STRONG> {{ $students->adresa }} </STRONG></p>
                    <p>Telefon: <STRONG> {{ $students->telefon }} </STRONG></p>
                    <p>Email: <STRONG> {{ $students->email }} </STRONG></p>
                    <p>Razred: <STRONG> {{ $students->razred }} </STRONG></p>
                    <p>Škola: <STRONG> {{ $students->skola }} </STRONG></p>
                </div> 
            </div> 
        </div> 
    </div>

    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Osnovni podaci roditelja</h3>
            </div>
            <div class="box-body">
                <div class="row col-md-12">
                    <p>Ime oca: <STRONG> {{ $students->ime}} {{$students->prezime}} </STRONG></p>
                    <p>Telefon oca: <STRONG> {{ $students->telefon }} </STRONG></p>
                    <p>E-mail oca: <STRONG> {{ $students->email }} </STRONG></p>   
                </div> 
            </div> 
            <div class="box-body">
                <div class="row col-md-12">              
                    <p>Ime majke: <STRONG> {{ $students->ime}} {{$students->prezime}} </STRONG></p>
                    <p>Telefon majke: <STRONG> {{ $students->telefon }} </STRONG></p>
                    <p>E-mail majke: <STRONG> {{ $students->email }} </STRONG></p>   
                </div> 
            </div> 
        </div> 
    </div>
</div> <!-- end of .row -->

<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Osnovni podaci polaznika</h3>
            </div>
            <div class="box-body">
                <div class="row col-md-12">
                    
                </div> 
            </div> 
        </div> 
    </div>
</div> <!-- end of .row -->

@endsection

@section('scripts-extra')

@endsection