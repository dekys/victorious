@extends('layouts.app')

@section('htmlheader_title')
	Kategorije usluga
@endsection

@section('contentheader_title')
	Kategorije usluga
@endsection

@section('header-extra')


<style>
    <!-- parsley CSS -->
    {!! Html::style('/css/parsley.css') !!}
    {!! Html::style('/plugins/colorpicker/bootstrap-colorpicker.css') !!}
</style>

<style type="text/css">
	div.inline { float:right; }
	.clearBoth { clear:both; }
</style>

@endsection


@section('main-content')
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Prikaz svih kategorija</h3>
			<button type="button" class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal"> <i class="fa fa-plus" aria-hidden="true"></i> Nova kategorija</button>
		</div>
		
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table id="example" class="table table-hover table-condensed table-striped ">
							<thead>
								<th>R.br.</th>
								<th>Naziv kategorije</th>
								<th>Opis</th>
								<th>Usluge</th>
								<th></th>
							</thead>

							<tbody>

								@php
									$i = ($categories->currentPage()-1)*$categories->perPage();
									
								@endphp

								@foreach ($categories as $category)

									<tr id='{{ $category->id }}'>
										<td style="width:60px">{{ ++$i }}. <div class="inline" style = "width:30px; height:30px; background:{{$category->color}};"> </div></td>
										<td style="width:300px"> {{ $category->naziv }} </td>
										<td> {{ $category->opis}} </td>
										<td style="width:200px"> 
										 	@foreach ( $category->service as $service)
												  - {{ $service->naziv}} <br>
											@endforeach 
											
										</td>
								
										<td class="td" align="right" style="width:150px">

											<a href="" 
												class="btn" 
												data-category_id="{{ $category->id }}" 
												data-category_naziv="{{ $category->naziv }}" 
												data-category_opis="{{ $category->opis }}"  
												data-category_color="{{ $category->color }}" 
												data-toggle="modal" 
												data-target="#editModal" 
												id="editButton"
												title="Izmena podataka" 
												><i class="fa fa-pencil-square-o"></i>
											</a>

											<a href="" data-category_id="{{ $category->id }}" class="btn" data-toggle="modal" data-target="#confirmDelete"> <i class="fa fa-trash-o"></i> </a>

										</td>
									</tr>
								@endforeach

							</tbody>  

						</table>

						<div class="text-center">
							{!! $categories->links(); !!}   

						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>

	{{csrf_field()}}

	{{-- Delete modal --}}
	<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Potvrda brisanja usluge</h4>
			  </div>
			  <div class="modal-body">
				<p>Da li ste sigurni da želite izbrisati odabranu uslugu?</p>
				<strong>Brisanjem ove kategorije biće obrisan i sve usluge i predavači koji joj pripadaju!</strong>
			  </div>
			  <div class="modal-footer">
			  	{!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
					<button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
				    <button type="submit" class="btn btn-primary">Izbriši</button>
				{!! Form::close() !!}
			  </div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal --> 


	{{-- Add modal --}}
	<div class="modal fade" id="AddModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			  	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Unos nove kategorija</h4>
			  	</div>
			      
   				{!! Form::open(['route' => 'category.store', 'class' => 'form-signin', 'data-parsley-validate' ]) !!}
			  		@include('administrators.services._form_category')
			  	{!! Form::close() !!}
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog --> 
	</div><!-- /.modal --> 

	
	{{-- Edit modal --}}
	<div class="modal fade" id="editModal" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			  	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Izmeni kategoriju</h4>
			  	</div>

				{!! Form::open(['route' => ['category.update',1], 'method' => 'PUT', 'id'=>'editForm', 'data-parsley-validate' =>'']) !!}
			  		@include('administrators.services._form_category')  
 			  	{!! Form::close() !!}
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal --> 
@endsection


@section('scripts-extra')

	{{-- DELETE  --}}
	<script type="text/javascript">

		$('#confirmDelete').on('show.bs.modal', function(e) 
		{
		    var categoryId = $(e.relatedTarget).data('category_id');
		    $("#delForm").attr('action', 'category/'+categoryId);
		});

	</script>

	{{-- edit modal--}}
	<script type="text/javascript">

		$('#editModal').on('show.bs.modal', function(e) {
			var categoryId = $(e.relatedTarget).data('category_id');
			var categoryNaziv = $(e.relatedTarget).data('category_naziv');
			var categoryOpis = $(e.relatedTarget).data('category_opis');
			var categoryColor = $(e.relatedTarget).data('category_color');

			var modal = $(this)
		    modal.find('#naziv').val(categoryNaziv)
		    modal.find('#opis').val(categoryOpis)
		    modal.find('#color').val(categoryColor)
		    modal.find('#id').val(categoryId)
		});

		$(document).ready(function() {
            $('#example').DataTable({
                "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
                },
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false                
            });
        });

		$(function() { $('#cp2').colorpicker(); }); 
	</script>

    <!-- parsley JS-->

    {!! Html::script('/js/parsley.min.js') !!}
    {!! Html::script('/plugins/colorpicker/bootstrap-colorpicker.js') !!} 

@endsection