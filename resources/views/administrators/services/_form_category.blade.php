<div class="modal-body">
    {{ Form::hidden('id', null, 
	            [
	            'id'							=> 'id',
	            ])
	        }}
    <div class="row">
        <div class="col-sm-6">
            {{ Form::text('naziv', null, 
	            [
	            'id'							=> 'naziv',
	            'class'                         => 'form-control form-group',
	            'placeholder'                   => 'Naziv kategorije',
	            'data-parsley-required-message' => 'Ovo polje je obavezno', 
	            'required'                      => '',  
	            'data-parsley-minlength'        => '5',
	            'data-parsley-maxlength'        => '50'
	            ])
	        }}
        </div>
        <div class="col-sm-2">
            {{ Form::color('color', null, 
	            [
	            'id'							=> 'color',
	            'class'                         => 'form-control form-group',
	            'data-parsley-required-message' => 'Ovo polje je obavezno', 
	            'required'                      => ''
	            ])
	        }}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            {{ Form::textarea('opis',  null,  
	            [
	            'id'							=> 'opis',
	            'resize'                        => 'none',
	            'size'                          => '30x5',
	            'data-parsley-required-message' => 'Ovo polje je obavezno',
	            'class'                         => 'form-control form-group', 
	            'placeholder'                  	=> 'Opis kategorije',
	            ])
	        }}
        </div>
    </div>
    
    <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" type="button">
            Odustani
        </button>
        <button class="btn btn-primary" type="submit">
            Prihvati
        </button>
    </div>
</div>