@extends('layouts.app')

@section('htmlheader_title')
	Usluge
@endsection

@section('contentheader_title')
	Usluge
@endsection

@section('main-content')

	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">
                @if ($q!==null)
                    Prikaz rezultata pretrage usluga za "{{$q}}"
                @else
                    Prikaz svih usluga
                @endif
			</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table id="example" class="table table-hover table-condensed table-striped">
							<thead>
								<th>R.br.</th>
								<th>Naziv usluge</th>
								<th>Kategorija</th>
								<th>Opis</th>
								<th>Cena</th>
								<th>Trajanje</th>
								<th> </th>
							</thead>

							<tbody>

                           		@php
                           			$i = ($services->currentPage()-1)*$services->perPage();
                           		@endphp

								@foreach ($services as $service)
									<tr>
										<td> {{ ++$i }}.</td>
										<td> {{ $service->naziv }} </td>
										<td> {{ $service->category->naziv }} </td>
										<td style="width:600px"> {{ $service->opis}} </td>
										<td> {{number_format($service->cena, 2, ',', '.')}} din.</td>  
										<td> {{ $service->trajanje}} - {{ $service->jm }} </td>
										<td class="td" align="right" style="width:150px">

											<a href="{{ route('service.edit', $service->id) }}"  class="btn" title="Izmena podataka"><i class="fa fa-pencil-square-o"></i></a> 
											<a href=""  class="btn" title="Izmena podataka" data-service_id="{{ $service->id }}" data-service_name="{{ $service->name }}" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete"> <i class="fa fa-trash-o"></i></a> 

										</td>
									</tr>
								@endforeach

							</tbody>  

						</table>

						<div class="text-center">
							{!! $services->appends(Request::only('q'))->links();  !!}                
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>

	<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Potvrda brisanja usluge</h4>
			  </div>
			  <div class="modal-body">
				<p>Da li ste sigurni da želite obrisati odabranu uslugu?</p>
			  </div>
			  <div class="modal-footer">
			  	{!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
					<button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
				    <button type="submit" class="btn btn-primary">Izbriši</button>
				{!! Form::close() !!}
			  </div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal --> 

	{{-- Edit modal --}}
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" >
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Izmena</h4>
			  </div>
				  <div class="modal-body">
	 
					    {!! Form::model($services, ['route' => ['service.update', 6], 'method' => 'PUT', 'data-parsley-validate' =>'']) !!}

					        @include('administrators.services._form')  
					    {!! Form::close() !!}   
				  </div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal --> 

@endsection

@section('scripts-extra')
	<script type="text/javascript">

		$('#confirmDelete').on('show.bs.modal', function(e) {
		    var serviceId = $(e.relatedTarget).data('service_id');
		    $("#delForm").attr('action', 'service/'+serviceId);
		});

        $(document).ready(function() {
        $('#example').DataTable({
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
            },
			"bPaginate": false,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"bAutoWidth": false,
            "searching": false

        });
        } );	
        
	</script>


@endsection
