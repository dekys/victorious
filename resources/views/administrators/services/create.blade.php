@extends('layouts.app')

@section('htmlheader_title')
    Unos nove usluge
@endsection

@section('contentheader_title')
    Unos nove usluge
@endsection

@section('header-extra')
    <!-- parsley CSS -->
    {!! Html::style('/css/parsley.css') !!}
@endsection

@section('main-content')
    {!! Form::open(['route' => 'service.store', 'class' => 'form-signin', 'data-parsley-validate' ]) !!}
        @include('administrators.services._form')     
    {!! Form::close() !!}   
@endsection

@section('scripts-extra')
    <script type="text/javascript">
        window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<span class="error-text"></span>',
            classHandler: function (el) {
                return el.$element.closest('input');
            },
            successClass: 'valid',
            errorClass: 'invalid'
        };
    </script>
   
    <!-- parsley JS-->
    {!! Html::script('/js/parsley.min.js') !!}
@endsection
