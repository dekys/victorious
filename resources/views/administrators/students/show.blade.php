@extends('layouts.app')

@section('htmlheader_title')
    Prikaz informacija o polazniku
@endsection

@section('contentheader_title')
    <div class="row col-md-8">
        Prikaz informacija o polazniku
            <a href="{{ action('AdministratorStudentController@edit', ["id" => $student->id] ) }}" class="btn btn-default btn-xs pull-right " title="Izmena podataka"> <i class="fa fa-pencil fa-sm"></i></a>
    </div>
@endsection

@section('header-extra')

@endsection

@section('main-content')
    <!-- Unos podataka o uceniku -->
<div class="row">
    <div class="col-md-4">
        <div class="box box-primary">        
            <div class="box-header with-border">
                <h3 class="box-title">Osnovni podaci polaznika</h3>
            </div>
            <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                    <table id="example" class="table table-striped">

                            <tbody>

                                    <tr>
                                        <td style="width:200px">Ime i prezime:</td>
                                        <td> <STRONG> {{ $student->ime }} {{ $student->prezime }}</STRONG> </td>
                                    </tr>
                                    <tr>
                                        <td>Adresa:</td>
                                        <td> <STRONG> {{ $student->adresa }} </STRONG> </td>
                                    </tr>
                                    <tr>
                                        <td>Telefon:</td>
                                        <td> <STRONG> {{ $student->telefon }} </STRONG> </td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td> <STRONG> {{ $student->email }} </STRONG> </td>
                                    </tr>


                                    @if ($student->person == 0)
                                        <tr>
                                            <td>Razred:</td>
                                            <td> <STRONG> {{ $student->razred }} </STRONG> </td>
                                        </tr>
                                        <tr>
                                            <td>Škola:</td>
                                            <td> <STRONG> {{ $student->skola }} </STRONG> </td>
                                        </tr>  
                                    @endif

                                    @if ($student->person == 1)
                                        <tr>
                                            <td>JMBG:</td>
                                            <td> <STRONG> {{ $student->jmbg }} </STRONG> </td>
                                        </tr>
                                        <tr>
                                            <td>LK:</td>
                                            <td> <STRONG> {{ $student->brlk }} {{ $student->pu }} </STRONG> </td>
                                        </tr>
                                    @endif

                            </tbody>  

                        </table>
                </div> 
            </div> 
            </div>
        </div> 
    </div>

    @if ($student->person == 0)
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Osnovni podaci roditelja</h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example" class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td style="width:200px">Ime oca:</td>
                                        <td> <STRONG> {{ $student->parent->ime_oca}} {{$student->parent->prezime_oca}} </STRONG> </td>
                                    </tr>
                                    <tr>
                                        <td>Telefon oca:</td>
                                        <td> <STRONG> {{ $student->parent->telefon_oca }} </STRONG> </td>
                                    </tr>
                                    <tr>
                                        <td>E-mail oca:</td>
                                        <td> <STRONG> {{ $student->parent->email_oca }} </STRONG> </td>
                                    </tr>
                                    <tr>
                                        <td style="width:200px">Ime majke:</td>
                                        <td> <STRONG> {{ $student->parent->ime_majke}} {{$student->parent->prezime_majke}} </STRONG> </td>
                                    </tr>
                                    <tr>
                                        <td>Telefon majke:</td>
                                        <td> <STRONG> {{ $student->parent->telefon_majke }} </STRONG> </td>
                                    </tr>
                                    <tr>
                                        <td>E-mail majke:</td>
                                        <td> <STRONG> {{ $student->parent->email_majke }} </STRONG> </td>
                                    </tr>
                                </tbody>  
                            </table>
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div>
</div> <!-- end of .row -->

<!-- Podaci o predmetima za koje je ucenik yainteresovan -->
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Predmeti</h3>
                    <button type="button" class="btn btn-default btn-xs pull-right" data-toggle="modal" data-target="#subjectModal" data-id='{{ $student->id }}' id="btnCreate"> <i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table">
                            <table id="mytable" class="table table-hover table-condensed table-striped">
                            <thead>
                                <th>Predmet</th>
                                <th>Nastavnik</th>
                                <th>Ocena</th>
                                <th>Napomena</th>
                                <th> </th>
                            </thead>

                            <tbody>

                                @foreach ($student->subjects as $subject)

                                    <tr id='{{ $subject->id }}'>
                                        <td> {{ $subject->name }} </td>
                                        <td> {{ $subject->teacher}} </td>
                                        <td> {{ $subject->mark}} </td>  
                                        <td> {{ $subject->comment}} </td>  
                                        <td></td>

                                        <td class="td" align="right" style="width:200px">
                                            <button type="button" class="btn btn-sm" data-toggle="modal" data-target="#subjectModal" data-id={{ $subject->id }} id="btnDelete"> <i class="fa fa-eye"></i></button>
                                            
                                            <button type="button" class="btn btn-sm" data-toggle="modal" data-target="#subjectModal" data-id='{{ $subject->id }}' data-name='{{ $subject->name }}' data-teacher='{{ $subject->teacher }}' data-mark='{{ $subject->mark }}' data-comment='{{ $subject->comment }}' id="btnEdit" > <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>

                                @endforeach

                            </tbody>  

                            </table>

                        </div>
                    </div>
                </div> 
            </div> 
        </div> 
    </div>
</div> <!-- end of .row -->
    @endif

{{ csrf_field() }}

{{-- modal --}}
<div class="modal fade bs-example-modal-lg" tabindex="-1" id="subjectModal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="title"> </h4>
      </div>

      <div class="modal-body">
        <input type="hidden" id="id" name="id" value="">

        <form id="forma">
          <div class="row">
            <div class="form-group col-md-5">
              <input type="text" class="form-control" id="name" name="name" value="" placeholder ="Naziv predmeta">
            </div>        
            <div class="form-group col-md-5">
              <input type="text" class="form-control" id="teacher" value="" placeholder ="Ime nastavnika">
            </div>
            <div class="form-group col-md-2">
              <input type="text" class="form-control" id="mark" value="" placeholder ="Ocena">
            </div>
          </div>           

          <div class="row">
            <div class="form-group col-md-12">
              <textarea rows="4" cols="50"class="form-control" id="comment" value="" placeholder ="Napomena"> </textarea>
            </div>        
          </div>              

         </form>

         <form id="forma_delete">
          <div class="form-group">
            <strong>Да ли сте сигурни да желите избрисати установу?</strong>
          </div>          
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="modal-btn-delete">Избриши</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="modal-btn-update">Сачувај измене</button>
        <button type="button" class="btn btn-success" data-dismiss="modal" id="modal-btn-create">Прихвати</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@section('scripts-extra')

    <script>

        $(document).ready(function() {
            /*priprema modala za IZMENU*/
            $(document).on('click', '#btnEdit', function(event) {                  
                $('#title').text('Izmena predmeta');
                $('#modal-btn-update').show('400');
                $('#modal-btn-delete').hide('400');
                $('#modal-btn-create').hide('400'); 
                $('#forma').show('400');
                $('#forma_delete').hide('400');

            });
            /*priprema modala za BRISANJE*/
            $(document).on('click', '#btnDelete', function(event) {
                $('#title').text('Brisanje predmeta');
                $('#modal-btn-update').hide('400');
                $('#modal-btn-delete').show('400');
                $('#modal-btn-create').hide('400');
                $('#forma').hide('400');
                $('#forma_delete').show('');
            });
            /*priprema modala za UNOS NOVOG*/
            $(document).on('click', '#btnCreate', function(event) {
                $('#title').text('Dodavanje novog predmeta');            
                $('#modal-btn-update').hide('400');
                $('#modal-btn-delete').hide('400');
                $('#modal-btn-create').show('400');
                $('#forma').show('400');
                $('#forma_delete').hide('400');

            });

            // KREIRANJE NOVOG SLOGA 
            $('#modal-btn-create').click(function(event) {
                var name = $('#name').val();
                var teacher = $('#teacher').val();
                var mark = $('#mark').val();
                var comment = $('#comment').val();
                var id = $('#id').val();

                $.ajax({
                    url: 'subject',
                    type: 'POST',
                    data: {
                        'id': id,
                        'name': name, 
                        'teacher': teacher, 
                        'mark': mark, 
                        'comment': comment, 
                        'dataType': 'json',
                        '_token': $('input[name=_token]').val()
                    },
                })
                .done(function() {
                    console.log("success");
                     location.reload(); // da bi pokayala istu paginaciju
                    toastr.success('Predmet je uspešno unet.', '', {positionClass: 'toast-bottom-right'})
                })
                .fail(function( jqXhr, json, errorThrown) {
   
                    var errors = jqXhr.responseJSON;
                    var errorsHtml= '';
                    $.each( errors, function( key, value ) {
                        errorsHtml += '<li>' + value[0] + '</li>'; 
                    });
                    toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown, {positionClass: 'toast-bottom-right'});
                })
                .always(function() {
                    console.log("complete");

                });
                
            });

            // BRISANJE SLOGA
            $('#modal-btn-delete').click(function(event) {
                var id = $('#id').val();

                $.ajax({
                    url: '{{ url('administrator/student/subject') }}' + '/' + id,
                    type: 'DELETE',
                    data: {
                        'id': id, 
                        '_token': $('input[name=_token]').val()
                    },
                })
                .done(function() {
                    console.log("success");
                    location.reload(); 
                    toastr.success('Predmet je uspešno izbrisan.', '', {positionClass: 'toast-bottom-right'})
                })
                .fail(function() {
                    console.log("error");
                })
                
                .always(function() {
                    console.log("complete");                    
                });

            });

            // IZMENA SLOGA
            $('#modal-btn-update').click(function(event) {
                var id = $('#id').val();
                var name = $('#name').val();
                var teacher = $('#teacher').val();
                var mark = $('#mark').val();
                var comment = $('#comment').val();

                $.ajax({
                    url: '{{ url('administrator/student/subject') }}' + '/' + id,
                    type: 'POST',
                    data: {
                        '_method': 'put',
                        'id': id, 
                        'name': name, 
                        'teacher': teacher, 
                        'mark': mark, 
                        'comment': comment, 
                        'dataType': 'json',
                        '_token': $('input[name=_token]').val()
                    },
                })
                .done(function() {
                    console.log("success");
                    location.reload(); // da bi pokayala istu paginaciju
                    $( "#mytable" ).load( "/administrator/student/subject/" + id +" #mytable" ); //učitava samo tabelu

                    toastr.success('Podaci su uspešno izmenjeni.', '', {positionClass: 'toast-bottom-right'})
                })
                .fail(function( jqXhr, json, errorThrown) {
   
                    var errors = jqXhr.responseJSON;
                    var errorsHtml= '';
                    $.each( errors, function( key, value ) {
                        errorsHtml += '<li>' + value[0] + '</li>'; 
                    });
                    toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown, {positionClass: 'toast-bottom-right'});
                })
                .always(function() {
                    console.log("complete");                    
                });
            });
        });
   
    </script>
    
    <script type="text/javascript">

        $('#subjectModal').on('show.bs.modal', function(e) {
            var Id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var teacher = $(e.relatedTarget).data('teacher');
            var mark = $(e.relatedTarget).data('mark');
            var comment = $(e.relatedTarget).data('comment');


            var modal = $(this);
            modal.find('#id').val(Id);
            modal.find('#name').val(name);
            modal.find('#teacher').val(teacher);
            modal.find('#mark').val(mark);
            modal.find('#comment').val(comment);

        });

    </script>

@endsection
