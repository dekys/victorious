@extends('layouts.app')

@section('htmlheader_title')
    Polaznici
@endsection

@section('contentheader_title')
    Polaznici
@endsection

@section('header-extra')

@endsection

@section('main-content')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"> 
                @if ($q!==null)
                    Prikaz rezultata pretrage polaznika za "{{$q}}"
                @else
                    Prikaz svih polaznika
                @endif

            </h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped">
                            <thead>
                                <th>R.br.</th>
                                <th>Ime i prezime</th>
                                <th>Razred</th>
                                <th>Predmet</th>
                                <th>Datum rođenja</th>
                                <th>Telefon</th>
                                <th>Adresa</th>
                                <th>Kurs</th>
                                <th>Balans</th>
                                <th> </th>
                            </thead>

                            <tbody>
                                @php
                                    $i = ($students->currentPage()-1)*$students->perPage();
                                @endphp
                                
                                @foreach ($students as $student)

                                    <tr id='{{ $student->id }}'>
                                        <td>{{ ++$i }}.</td>
                                        <td> <a href="{{ route('student.show', $student->id) }}" title="Prikaži detalje">{{ $student->ime }} {{ $student->prezime }}</a></td>
                                        <td> {{ $student->razred}} </td>  
                                        <td> 
                                            @foreach ($student->subjects as $subject)
                                                {{$subject->name}} <br>
                   
                                            @endforeach

                                        </td>  
                                        <td> {{ date('d.m.Y.', strtotime( $student->rodjen)) }} </td>
                                        <td> {{ $student->telefon}} </td>
                                        <td> {{ $student->adresa}} </td>  
                                        <td> </td>
                                        <td>{{number_format($i*1509.325/1.2, 2, ',', '.')}}</td>
                                        <td class="td" align="right" style="width:200px">
                                        
                                        <a href="{{ route('student.edit', $student->id) }}" class="btn" title="Izmena podataka"><i class="fa fa-pencil-square-o"></i></a> 
                                        <a href="{{ route('student.prijava', $student->id) }}" class="btn" title="Štampanje prijave"><i class="fa fa-file-text-o" aria-hidden="true"></i></a> 
                                        <a href="" title="Brisanje podatka" data-student_id="{{ $student->id }}" data-student_name="{{ $student->name }}" class="btn" data-toggle="modal" data-target="#confirmDelete"> <i class="fa fa-trash-o"></i></a> 


                                        </td>
                                    </tr>
                                    
                                @endforeach

                            </tbody>  

                        </table>

                        <div class="text-center">
                            {!! $students->appends(Request::only('q'))->links(); !!}                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>


    {{-- MODAL DELETE --}}
    <div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Potvrda brisanja polaznika</h4>
              </div>
              <div class="modal-body">
                <p>Da li ste sigurni da želite obrisati odabranu polaznika?</p>
              </div>
              <div class="modal-footer">
                {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Izbriši</button>
                {!! Form::close() !!}
              </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 

@endsection

@section('scripts-extra')

    <script type="text/javascript">



        $('#confirmDelete').on('show.bs.modal', function(e) {
            var studentId = $(e.relatedTarget).data('student_id');
             $("#delForm").attr('action', 'student/'+studentId);
        });

        $(document).ready(function() {
            $('#example').DataTable({
                "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
                },
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "searching": false

            });
        } );
        
     </script>

@endsection
