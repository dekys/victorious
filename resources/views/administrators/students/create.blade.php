@extends('layouts.app')

@section('htmlheader_title')
    Unos novog polaznika
@endsection

@section('contentheader_title')
    Unos novog polaznika
@endsection

@section('header-extra')
<!-- parsley CSS -->
{!! Html::style('/css/parsley.css') !!}
    {!! Html::style('/plugins/datepicker/datepicker3.css') !!}
@endsection

@section('main-content')
    {!! Form::open(['route' => 'student.store', 'data-parsley-validate']) !!}
        @include('administrators.students._form')     
    {!! Form::close() !!}   
@endsection

@section('scripts-extra')
    <script type="text/javascript">
        window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<span class="error-text"></span>',
            classHandler: function (el) {
                return el.$element.closest('input');
            },
            successClass: 'valid',
            errorClass: 'invalid'
        };

        function myFunction() {
            if ($("#person").val() == 1) {
                $('#djak').hide(400);
                $('#roditelj').hide(400);                
                $('#odrasli').show(400);

                $('#razred').val(null);
                $('#skola').val(null);
            } else {
                $('#djak').show(400);
                $('#roditelj').show(400);
                $('#odrasli').hide(400);

                $('#jmbg').val(null);
                $('#pu').val(null);
                $('#brlk').val(null);
            };
        };

        $(function () {
            $('#birthday').datepicker({
                format: "dd.mm.yyyy",
                language: "rs-latin"
            });        
        });
    </script>
    <!-- parsley JS-->
    {!! Html::script('/js/parsley.min.js') !!}
    {!! Html::script('/js/rs.js') !!}
    {!! Html::script('/plugins/datepicker/bootstrap-datepicker.js') !!}
    {!! Html::script('/plugins/ckeditor/ckeditor.js') !!} 
   
@endsection
