<!DOCTYPE html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<head>	
		<title>Prijava polaznika - predmet</title>

		<style>
			@page { 
				margin-top: 2cm; 
				margin-left: 2cm; 
				margin-right: : 2cm; 
				margin-bottom: : 2cm; 
			}
			body { font-family: DejaVu Sans; font-size: 12px;}
			table {border-collapse: collapse}
			th, td {
				height: 40px}
		</style>

		<div style="width:660px" align="center">
			<img src="{{ public_path() . '/img/logo.png' }}" style="width:300px" align="center">
		</div>

		<br>

		<h1 style="width:660px" align="center">PRIJAVA</h1>
		{{-- <h3 style="width:660px" align="center">za predmet</h3> --}}

		<?php $i = 0 ?>
		@foreach ($student->subjects as $subject)
			<p> <strong>{{++$i}}. {{ $subject->name }} </strong> </p>
		@endforeach

	</head>
	

    <body>

		<table>		
			<tbody>
			    <tr>
			        <td>Ime i prezime kandidata: </td> 
			        <td> <strong> {{ strtoupper($student->ime) }} {{ strtoupper($student->prezime) }} </strong> </td>
			    </tr>
			    <tr>
				    <td>Datum rođenja:</td>
				    <td><strong> {{ Carbon\Carbon::parse($student->rodjen)->format('d.m.Y.') }}</strong></td>
			    </tr>
			</tbody>
		</table>


		<div>
			<p>Cena kursa je {{"xxx,xx"}} dinara uz mogućnost plaćanja na više mesečnih rata.</p>
			<p>Predavač, predmetni nastavnik {{"xxxxxxxxxx xxxxxx"}}.</p>
			<p>Kontakt telefon mobilni/fiksni: <strong> {{ $student->telefon }} </strong></p>
			<p>Email:<strong> {{ $student->email }} </strong></p>
			<p>Adresa stanovanja: <strong> {{ $student->adresa }} </strong></p>
			<p>Napomena:</p>
			<p><strong> {!! $student->napomena !!} </strong></p>
		</div>




		<br><br><br>
		<table style="width:660px">
			<tbody>
				<tr>
				    <td><i><small>*Potpisom ove prijave smatramo da ste obavešteni da je objekat  Edukativnog kutka „Victorious“ pod 24h video nadzorom, kao i da ste saglasni da se fotografije vaše dece mogu objaviti na FB stranici i veb stranici www.victorious.co.rs.</small></i></td>
			    </tr>
			</tbody>
		</table>


		<table style="margin-top: 30px">
			<tr>
				<td style="width:300px">U Kruševcu, dana {{ Carbon\Carbon::now()->format('d.m.Y.') }}</td>
				<td style="width:60px"> </td>
				<td align="center" style="width:300px">Kandidat</td>
			</tr>
			<tr>
				<td style="width:300px"> </td>
				<td style="width:60px"> </td>
				<td align="center" style="width:300px">	________________________________</td>
			</tr>
		</table>

    </body>
</html>