<!DOCTYPE html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<head>	
		<title>Prijava polaznika - predmet</title>

		<style>
			@page { 
				margin-top: 2cm; 
				margin-left: 2cm; 
				margin-right: : 2cm; 
				margin-bottom: : 2cm; 
			}
			body { font-family: DejaVu Sans; font-size: 12px;}
			table {border-collapse: collapse}
			th, td {
				/*border: 1px solid black; */
				height: 40px}
		</style>

		<div style="width:660px" align="center">
			<img src="{{ public_path() . '/img/logo.png' }}" style="width:300px" align="center">
		</div>

		<br>

		<h1 style="width:660px" align="center">PRIJAVA</h1>
		<h3 style="width:660px" align="center">za predmet</h3>

		<?php $i = 0 ?>
		@foreach ($student->subjects as $subject)
			<p> <strong>{{++$i}}. {{ $subject->name }} </strong> </p>
		@endforeach

	</head>
	

    <body>

		<table>		
			<tbody>
			    <tr>
			        <td> Ime i prezime deteta: </td> 
			        <td> <strong> {{ strtoupper($student->ime) }} {{ strtoupper($student->prezime) }} </strong> </td>
			    </tr>
			    <tr>
				    <td>Datum rođenja:</td>
				    <td><strong> {{ Carbon\Carbon::parse($student->rodjen)->format('d.m.Y.') }}</strong></td>
			    </tr>
				<tr>
				    <td>Razred:</td>
				    <td><strong> {{ $student->razred }} </strong></td>
			    </tr>
			    <tr>
				    <td>Škola:</td>
				    <td><strong> {{ $student->skola }} </strong></td>
			    </tr>
			    <tr>
				    <td>Predmetni nastavnik:</td>
				    <td><strong>   </strong></td>
			    </tr>		    
			    <tr>
				    <td>Trenutna ocena u školi:</td>
				    <td><strong>   </strong></td>
			    </tr>
			    
			    <tr>
				    <td>Napomena:</td>
				    <td><strong>  {!! $student->napomena !!} </strong></td>
			    </tr>


			    <tr>
				    <td>Ime oca:</td>
				    <td><strong> {{ $student->parent->ime_oca }} {{ $student->parent->prezime_oca }} </strong></td>	
			    </tr>
			    <tr>
					<td>Kontakt telefon:</td>
				    <td><strong> {{ $student->parent->telefon_oca }} </strong></td>
				    <td>Email:</td>
				    <td><strong> {{ $student->parent->email_oca }} </strong></td>
			    </tr>

			    <tr>
				    <td>Ime majke:</td>
				    <td><strong> {{ $student->parent->ime_majke }} {{ $student->parent->prezime_majke }} </strong></td>
				</tr>
			    <tr>
					<td>Kontakt telefon:</td>
				    <td><strong> {{ $student->parent->telefon_majke }} </strong></td>
					<td>Email:</td>
				    <td><strong> {{ $student->parent->email_majke }} </strong></td>
			    </tr>
			</tbody>
		</table>
		<br><br>
		<table style="width:660px">
			<tbody>
				<tr>
				    <td><i><small>*Potpisom ove prijave smatramo da ste obavešteni da je objekat  Edukativnog kutka „Victorious“ pod 24h video nadzorom, kao i da ste saglasni da se fotografije vaše dece mogu objaviti na FB stranici i veb stranici www.victorious.co.rs.</small></i></td>
			    </tr>
			</tbody>
		</table>

		<br>
		<br>
	  	<br>
		<br>

		<table>
			<tr>
				<td style="width:300px">U Kruševcu, dana {{ Carbon\Carbon::now()->format('d.m.Y.') }}</td>
				<td style="width:60px"> </td>
				<td align="center" style="width:300px">Roditelj</td>
			</tr>
			<tr>
				<td style="width:300px"> </td>
				<td style="width:60px"> </td>
				<td align="center" style="width:300px">	________________________________</td>
			</tr>
		</table>

    </body>
</html>