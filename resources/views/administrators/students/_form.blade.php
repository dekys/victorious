<!-- Unos podataka o uceniku -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            {{--
            <div class="box-header with-border">
                --}}
            {{--
            </div>
            --}}
            <div class="box-body">
                <h4>
                    Osnovni podaci polaznika
                </h4>
                <div class="row">
                    <div class="col-sm-4">
                        {{Form::select('person', ['0' => 'Dete', '1' => 'Odrasla osoba'], null,
                            [
                                'id'                        =>'person',
                                'onChange'                  =>"myFunction()",
                                'class'                     =>'form-control form-group'
                            ])
                        }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                        {{ Form::text('ime', null,
                            [
                            'class'                         => 'form-control form-group',
                            'placeholder'                   =>'Ime polaznika',
                            'required'                      =>'',
                            'data-parsley-minlength'        => '2',
                            'data-parsley-maxlength'        => '32'
                            ])
                        }}
                    </div>
                    <div class="col-sm-4">
                        {{-- {!! Form::label('prezime', 'Prezime * :') !!} --}}
                        {{ Form::text('prezime', null, 
                            [
                            'class'                         => 'form-control form-group',
                            'placeholder'                   =>'Prezime polaznika',
                            'required'                      =>'',
                            'maxlength'                     =>"50"
                            ])
                        }}
                    </div>
                    <div class="col-sm-4">
                        {{-- {!! Form::label('rodjen', 'Datum rođenja * :') !!} --}}
                        {{ Form::text('rodjen',  $datum,  
                            [
                            'class'                         =>'form-control form-group', 
                            'id'                            =>'birthday', 
                            'placeholder'                   =>'Datum rođenja',
                            'required'                      =>'', 
                            'maxlength'                     =>"20",
                            ])
                        }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        {{-- {{ Form::label('adresa', 'Adresa prebivališta:') }} --}}
                        {{ Form::text('adresa', null, 
                            [
                            'class'                         => 'form-control form-group',
                            'placeholder'                   =>'Adresa prebivališta',
                            'required'                      =>'', 
                            'maxlength'                     =>"50"
                            ])
                        }}
                    </div>
                    <div class="col-sm-4">
                        {{-- {{ Form::label('telefon', 'Kontakt telefon:') }} --}}
                        {{ Form::text('telefon', null, 
                            [
                            'class'                         =>'form-control form-group', 
                            'placeholder'                   =>'Broj telefona',
                            'required'                      =>'',
                            'maxlength'                     =>"50"
                            ]) 
                        }}
                    </div>
                    <div class="col-sm-4">
                        {{-- {{ Form::label('email', 'Email adresa:') }} --}}
                        {{ Form::text('email', null, 
                            [
                            'class'                         => 'form-control form-group', 
                            'placeholder'                   =>'Email adresa',
                            'data-type-email-message'       =>'Unesite validnu email adresu',
                            'required'                      =>'',
                            'data-parsley-type'             => 'email'
                            ]) 
                        }}
                    </div>
                </div>
                <div class="row" id="djak">
                    <div class="col-sm-4">
                        {{-- {{ Form::label('razred', 'Razred:') }} --}}
                            {{ Form::text('razred', null, 
                                [
                                'id'                        =>'razred',
                                'class'                     =>'form-control form-group',
                                'placeholder'               =>'Razred',
                                'maxlength'                 =>"50",
                                'data-parsley-pattern'      =>"^[0-9]*$"
                                ])
                            }}
                    </div>
                    <div class="col-sm-4">
                        {{-- {{ Form::label('skola', 'Škola:') }} --}}
                            {{ Form::text('skola', null, 
                                [
                                'id'                        =>'skola',
                                'class'                     =>'form-control form-group', 
                                'placeholder'               =>'Škola',
                                'maxlength'                 =>"50"
                                ]) 
                            }}
                    </div>
                </div>
                <div class="row" id="odrasli" style="display: none;">
                    <div class="col-sm-4">
                        {{-- {{ Form::label('jmbg', 'JMBG:') }} --}}
                            {{ Form::text('jmbg', null, 
                                [
                                'id'                        => 'jmbg',
                                'class'                     => 'form-control form-group',
                                'placeholder'               => 'Jedinstveni matični broj građana',
                                'maxlength'                 => '13',
                                'minlength'                 => '13'
                                ])
                            }}
                    </div>
                    <div class="col-sm-4">
                        {{-- {{ Form::label('brlk', 'Broj lične karte:') }} --}}
                            {{ Form::text('brlk', null, 
                                [
                                    'id'                        => 'brlk',
                                    'class'                     => 'form-control form-group', 
                                    'placeholder'               => 'Broj lične karte',
                                    'maxlength'                 => "50"
                                ]) 
                            }}
                    </div>
                    <div class="col-sm-4">
                        {{-- {{ Form::label('pu', 'Policijska uprava:') }} --}}
                            {{ Form::text('pu', null, 
                                [
                                'id'                            => 'pu',
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   => 'Policijska uprava koja je izdala ličnu kartu'
                                ]) 
                            }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{-- {!! Form::label('rodjen', 'Datum rođenja * :') !!} --}}
                        {{ Form::textarea('napomena',  null,  
                            [
                            'resize'                            => 'none',
                            'size'                              => '30x5',
                            'class'                             => 'form-control form-group ckeditor', 
                             'placeholder'                      =>'Napomena',
                            ])
                        }}
                    </div>
                </div>
                <div id="roditelj">
                    <h4>
                        Podaci oca
                    </h4>
                    <div class="row">
                        <div class="col-sm-2">
                            {{-- {{ Form::label('adresa', 'Adresa prebivališta:') }} --}}
                            {{ Form::text('parent[ime_oca]', null, 
                                [
                                'class'                         =>'form-control form-group',
                                'placeholder'                   =>'Ime oca',
                                'maxlength'                     =>"50"
                                ])
                            }}
                        </div>
                        <div class="col-sm-2">
                            {{-- {{ Form::label('adresa', 'Adresa prebivališta:') }} --}}
                            {{ Form::text('parent[prezime_oca]', null, 
                                [
                                'class'                         =>'form-control form-group',
                                'placeholder'                   =>'Prezime oca',
                                'maxlength'                     =>"50"
                                ])
                            }}
                        </div>
                        <div class="col-sm-4">
                            {{-- {{ Form::label('telefon', 'Kontakt telefon:') }} --}}
                            {{ Form::text('parent[telefon_oca]', null, 
                                [
                                'class'                         =>'form-control form-group', 
                                'placeholder'                   =>'Broj telefona oca',
                                'maxlength'                     =>"50"
                                ]) 
                            }}
                        </div>
                        <div class="col-sm-4">
                            {{-- {{ Form::label('email', 'Email adresa:') }} --}}
                            {{ Form::text('parent[email_oca]', null, 
                                [
                                'class'                         =>'form-control form-group', 
                                'placeholder'                   =>'Email adresa oca',
                                'data-parsley-type'             => 'email'
                                ]) 
                            }}
                        </div>
                    </div>
                    <h4>
                        Podaci majke
                    </h4>
                    <div class="row">
                        <div class="col-sm-2">
                            {{-- {{ Form::label('adresa', 'Adresa prebivališta:') }} --}}
                            {{ Form::text('parent[ime_majke]', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   =>'Ime majke',
                                'maxlength'                     =>"50"
                                ])
                            }}
                        </div>
                        <div class="col-sm-2">
                            {{-- {{ Form::label('adresa', 'Adresa prebivališta:') }} --}}
                            {{ Form::text('parent[prezime_majke]', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   =>'Prezime majke',
                                'maxlength'                     =>"50"
                                ])
                            }}
                        </div>
                        <div class="col-sm-4">
                            {{-- {{ Form::label('telefon', 'Kontakt telefon:') }} --}}
                            {{ Form::text('parent[telefon_majke]', null, 
                                [
                                'class'                         =>'form-control form-group', 
                                'placeholder'                   =>'Broj telefona majke',
                                'maxlength'                     =>"50"
                                ]) 
                            }}
                        </div>
                        <div class="col-sm-4">
                            {{-- {{ Form::label('email', 'Email adresa:') }} --}}
                            {{ Form::text('parent[email_majke]', null, 
                                [
                                'class'                         =>'form-control form-group', 
                                'placeholder'                   =>'Email adresa majke',
                                'data-parsley-type'             =>'email'
                                ]) 
                            }}
                        </div>
                    </div>
                </div>
                @if (!Request::is('*/edit'))
                <div class="row col-md-4">
                    {{Form::checkbox('create_user', '1', false)}} {{ 'Kreiraj korisnika' }}
                </div>
                @else
                <br>
                    @endif
                    <!-- Dugmici -->
                    <div class="row">
                        <div class="col-md-12 col-md-offset-8">
                            <div class="row">
                                <div class="col-md-2">
                                    {{ Form::button('Poništi', array('class' => 'btn btn-primary  btn-block', 'type'=>'reset')) }}
                                </div>
                                <div class="col-md-2">
                                    {{ Form::button('Prihvati',  array('class' => 'btn btn-primary btn-block', 'type'=>'submit')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of .row dugmici-->
                </br>
            </div>
        </div>
    </div>
</div>
<!-- end of .row -->
