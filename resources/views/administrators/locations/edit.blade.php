@extends('layouts.app')

@section('htmlheader_title')
    Izmena lokacija
@endsection

@section('contentheader_title')
    Izmena lokacija
@endsection

@section('header-extra')
    <!-- parsley CSS -->
    {!! Html::style('/css/parsley.css') !!}

@endsection


@section('main-content')

    {!! Form::model($location, ['route' => ['location.update', $location->id], 'method' => 'PUT', 'data-parsley-validate' =>'']) !!}
        @include('administrators.locations._form')  
    {!! Form::close() !!}   
    
@endsection


@section('scripts-extra')
    <script type="text/javascript">
        window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<span class="error-text"></span>',
            classHandler: function (el) {
                return el.$element.closest('input');
            },
            successClass: 'valid',
            errorClass: 'invalid'
        };
    </script>

    <!-- parsley JS-->
    {!! Html::script('/js/parsley.min.js') !!}

@endsection