<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            {{-- <div class="box-header with-border"> --}}
            {{-- </div> --}}
            <div class="box-body">
                <h4 class="box-title">Lokacija</h4>


                    <div class="row">
                        <div class="col-sm-4">
                            {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                            {{ Form::text('name', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   => 'Naziv lokacije',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '',  
                                //'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                'data-parsley-minlength'        => '5',
                                'data-parsley-maxlength'        => '50'
                                ])
                            }} 
                        </div>

                        <div class="col-sm-2">
                            {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                            {{ Form::text('capacity', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   => 'Kapacitet',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '',  
                                //'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                //'data-parsley-minlength'        => '5',
                                //'data-parsley-maxlength'        => '50'
                                ])
                            }} 
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                            {{ Form::text('address', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   => 'Adresa lokacije',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '',  
                                //'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                'data-parsley-minlength'        => '5',
                                'data-parsley-maxlength'        => '50'
                                ])
                            }} 
                        </div>                        
                    </div>


                    <div class="row">
                        <div class="col-sm-12">
                            {{-- {!! Form::label('rodjen', 'Datum rođenja * :') !!} --}}
                            {{ Form::textarea('note',  null,  
                                [
                                'resize'                        => 'none',
                                'size'                          => '30x5',
                                'data-parsley-required-message' => 'Ovo polje je obavezno',
                                'class'                         => 'form-control form-group', 
                                 'placeholder'                  => 'Opis lokacije',
                                ])
                            }} 
                        </div>
                    </div>

 
               

                <!-- Dugmici -->
                <div class="row">
                    <div class="col-md-12 col-md-offset-8">
                        <div class="row">
                            <div class="col-md-2">
                                {{ Form::button('Poništi', array('class' => 'btn btn-primary  btn-block', 'type'=>'reset')) }} 
                            </div>
                            <div class="col-md-2">
                                {{ Form::button('Prihvati',  array('class' => 'btn btn-primary btn-block', 'type'=>'submit')) }} 
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- end of .row dugmici-->
            </div> 
        </div> 
    </div>
</div> <!-- end of .row -->

   
