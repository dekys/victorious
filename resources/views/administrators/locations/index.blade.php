@extends('layouts.app')

@section('htmlheader_title')
	Lokacije
@endsection

@section('contentheader_title')
	Lokacije
@endsection

@section('main-content')

	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Prikaz svih usluga</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table id="example" class="table table-hover table-condensed table-striped">
							<thead>
								<th>R.br.</th>
								<th>Naziv lokacije - učionice</th>
								<th>Kapacitet</th>
								<th>Napomena</th>
								<th> </th>
							</thead>

							<tbody>

                           		@php
                           			$i = ($locations->currentPage()-1)*$locations->perPage();
                           		@endphp

								@foreach ($locations as $location)
									<tr>
										<td> {{ ++$i }}.</td>
										<td> {{ $location->name }} </td>
										<td> {{ $location->capacity }} </td>
										<td> {{ $location->note }} </td>
										<td class="td" align="right" style="width:150px">

											<a href="{{ route('location.edit', $location->id) }}"  class="btn btn-sm" title="Izmeni lokaciju"><i class="fa fa-pencil"></i></a> 
											<a class="btn btn-sm" data-location_id="{{ $location->id }}" data-location_name="{{ $location->name }}"  data-toggle="modal" data-target="#confirmDelete" title="Izbriši lokaciju"><i class="fa fa-trash"></i></a> 
										</td>
									</tr>
								@endforeach

							</tbody>  

						</table>

						<div class="text-center">
							{!! $locations->links(); !!}                
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
<!--  -->
	<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Potvrda brisanja lokacija</h4>
			  </div>
			  <div class="modal-body">
				<p>Da li ste sigurni da želite obrisati odabranu lokaciju?</p>
			  </div>
			  <div class="modal-footer">
			  	{!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
					<button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
				    <button type="submit" class="btn btn-primary">Izbriši</button>
				{!! Form::close() !!}
			  </div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal --> 


@endsection

@section('scripts-extra')
	<script type="text/javascript">

		$('#confirmDelete').on('show.bs.modal', function(e) {
		    var locationId = $(e.relatedTarget).data('location_id');
		    $("#delForm").attr('action', 'location/'+locationId);
		});

        $(document).ready(function() {
        $('#example').DataTable({
            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
            },
			"bPaginate": false,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"bAutoWidth": false
        });
        } );	
        
	</script>


@endsection
