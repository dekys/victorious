@extends('layouts.app')

@section('htmlheader_title')
    Kalendar 
@endsection

@section('contentheader_title')
    Kalendar
@endsection

@section('header-extra')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css"/>

    <style>
        #calendar {
            height: 80%;
        }
    </style>


@endsection

@section('main-content')

<div>
    <div class="btn-group" role="group" aria-label="...">
        <ul class="nav nav-pills" id="rowTab">
            @foreach ($locations as $location)
                
                <a href="{{$location->id}}" class="btn btn-default btn-sm {{ Request::is("dashboard/$location->id") ? 'active' : '' }}"">{{$location->name}}</a>
            @endforeach
        </ul>
    </div>

 
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                     <div class="tab-content">
                        @foreach ($locations as $location)
                            <div id={{$location->id}} class="tab-pane fade in active">
                                <div id="calendar" class="fc fc-unthemed fc-ltr"> </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('scripts-extra')
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="{{ asset('plugins/fullcalendar/locale/sr.js') }}" type="text/javascript"></script>

    <script>

        $(document).ready(function() {

           var $calendar = $('#calendar').fullCalendar(
            {   
                snapDuration: '00:15:00',
                editable: true,
                minTime: "07:00:00",
                maxTime: "22:00:00",
                contentHeight: 800,
                header: {
                    left: 'today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay prev,next'
                    },
                defaultView: 'agendaWeek',
                events: [

                        @foreach($events as $event)
                        {
                            id : '{{ $event->id }}',
                            title : '{{ $event->title }}',
                            start : '{{ $event->start }}',
                            end : '{{ $event->end }}',
                            description : '{{ @isset ($event->course->teacher->ime)  ? $event->course->teacher->ime .' '. $event->course->teacher->prezime : "" }}',
                            url: '{{ url('/event') }}' + '/' + '{{ $event->id }}',
                            color: '{{ $event->background_color }}',
                            textColor: '#222d32',
                        },
                        @endforeach
                ],
                    eventRender: function(event, element) { 
                         element.find('.fc-title').append("<br/>" + event.description); 
                     },

                eventResize: function(event, delta, revertFunc) {
                    start = moment(event.start, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
                    end = moment(event.end, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')

                    $.ajax({
                        type: "POST",
                        url: '{{ url('/event') }}' + '/' + event.id,
        
                        data: { 
                            'id': event.description,
                            'title': event.title,
                            'start': start,
                            'end': end,
                            '_method': 'put',
                            'dataType': 'json',
                            '_token': $('input[name=_token]').val()
                        }
                    })
                },



                eventDrop: function(event, delta, revertFunc) {
                    start = moment(event.start, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
                    end = moment(event.end, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')

                    $.ajax({
                        type: "POST",
                        url: '{{ url('/event') }}' + '/' + event.id,
        
                        data: { 
                            'id': event.description,
                            'title': event.title,
                            'start': start,
                            'end': end,
                            '_method': 'put',
                            'dataType': 'json',
                            '_token': $('input[name=_token]').val()
                        }
                    })
                }
            });
        });

    </script>
@endsection


