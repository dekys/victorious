@extends('layouts.auth')

@section('htmlheader_title')
    Stranica nije pronađena
@endsection

@section('contentheader_title')
    404 Error Page
@endsection

@section('$contentheader_description')
@endsection

@section('content')

<div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Stranica nije pronađena.</h3>
        <p>
            Nije moguće pronaći stranicu koju ste tražili.
            Možete se vratiti na <a href='{{ url('/') }}'>početnu stranu</a>.
        </p>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection