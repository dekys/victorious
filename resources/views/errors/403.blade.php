@extends('layouts.auth')

@section('htmlheader_title')
    Nemate dozvolu pristupa
@endsection

@section('contentheader_title')
    403 Error Page
@endsection

@section('$contentheader_description')
@endsection

@section('content')

<div class="error-page">
    <h2 class="headline text-yellow"> 403</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Nemete dovoljno privilegija.</h3>
        <p>
            Vaš nalog nema privilegija za pristup ovom delu portala.
            Možete se vratiti na <a href='{{ url('/') }}'>početnu stranu</a>.
        </p>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection