@extends('layouts.app')

@section('htmlheader_title')
    Poruke
@endsection

@section('contentheader_title')
    Poruke
@endsection

@section('header-extra')
@endsection

@section('main-content')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Prikaz svih poruka</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped">
                            <thead>
                                <th>R.br.</th>
                                <th>Poruka od</th>
                                <th>Subject</th>
                                <th>Vreme</th>
                            </thead>

                            <tbody>
                                @foreach ($messages as $message)
                                    <tr style="cursor: pointer;" data-toggle="modal" 
                                    data-toggle="modal" 
                                    data-target="#readMsg"
                                    data-sender="{{$message->sender->firstname }} {{ $message->sender->lastname }}" 
                                    data-body ="{{ $message->body }}"
                                    data-subject ="{{ $message->subject }}"
                                    data-msgid ="{{ $message->id }}"
                                    >
                                        <td> {{ $message->id }}.</td>
                                        <td> {{ $message->sender->firstname }} {{ $message->sender->lastname }}</td>
                                        <td> {{ $message->subject }}</a></td>
                                        <td> {{ $message->created_at->diffforhumans() }} </td>
                                    </tr>
                                @endforeach

                            </tbody>  

                        </table>

                        <div class="text-center">
                            {{-- {!! $messages->links(); !!}     --}}            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
<div id="readMsg" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Poruka od <span id="msgfrom"></span></h4>
      </div>
      <div class="modal-body">
        <h3 id="msgsubject"></h3>
        <p id="msgbody"></p>
      </div>
      <div class="modal-footer">
         {!! Form::open(['method' => 'DELETE', 'id'=>'delMsg']) !!}
        {!! Form::submit('Obriši', ['class' => 'btn btn-danger']) !!}
        <button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
        {{ Form::close() }}
      </div>
    </div>

  </div>
</div>


@endsection

@section('scripts-extra')

    <script type="text/javascript">
        $('#readMsg').on('show.bs.modal', function(e) {
            var from = $(e.relatedTarget).data('sender');
            var subject = $(e.relatedTarget).data('subject');
            var body = $(e.relatedTarget).data('body');
            var message_id = $(e.relatedTarget).data('msgid');

            $("#msgfrom").text(from);
            $("#msgsubject").text(subject);
            $("#msgbody").text(body);
            $("#delMsg").attr('action', '/message/'+message_id);
        });
    </script>
@endsection