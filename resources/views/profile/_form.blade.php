<div class="row">
	<div class="col-md-12">

		<div class="col-sm-6">
			{!! Form::text('firstname', null, 
			['class' => 'form-control form-group', 
			'placeholder' => 'Ime', 
			'required' => '']) !!}
		</div>	

		<div class="col-sm-6">
			{!! Form::text('lastname', null, 
			['class' => 'form-control form-group', 
			'placeholder' => 'Prezime', 
			'required' => '']) !!}
		</div>	

		<div class="col-sm-12">
			{!! Form::text('email', null, 
			['class' => 'form-control form-group', 
			'placeholder' => 'E-Mail', 
			'required' => '']) !!}
		</div>
		
		<div class="col-sm-6">
			<table class="table table-hover">
				@foreach($rol as $role)
				<tr>
					<td class="col-md-10">
						{!! Form::label('role', $role->display_name); !!}	
					</td>
					<td class="col-md-2">
						{!! Form::radio('role', $role->id) !!}
					</td>
				</tr>
			@endforeach
		</table>
		</div>
			
			
		<div class="col-sm-12">
			{!! Form::submit('Dodaj Korisnika', 
			['class' => 'form-control btn btn-primary form-group']); !!}
		</div> 

	
	</div>
</div>
