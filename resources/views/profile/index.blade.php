@extends('layouts.app')
@section('htmlheader_title')
Moj profil
@endsection
@section('contentheader_title')
Moj profil
@endsection
@section('header-extra')
@endsection
@section('main-content')
<div class="row">
    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile" aling = "center">
                <input type="file" id="imgupload" style="display:none"/>
                <img class="profile-user-img img-responsive img-circle" src="{{ asset($profile->image) }}" alt="User profile picture" width="100">
                <h3 class="profile-username text-center">{{ $profile->firstname." ".$profile->lastname }}</h3>
                @foreach($profile->roles as $roles)
                <p class="text-muted text-center">{{ $roles->display_name }}</p>
                <?php $uloga = "$roles->display_name" ?>
                @endforeach
                <form enctype="multipart/form-data" action="/profile/{{ $profile->id }}" method="POST">
                    <label>Postavite profilnu sliku</label>
                    <div class="form-group">
                        <input type="file" name="avatar">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Postavi Sliku</button>
                </form>
                {{-- <hr>
                <button type="button" class="btn btn-primary col-md-12" data-toggle="modal" data-target="#poruka">Pošalji poruku<i class="fa fa-envelope-o pull-left" aria-hidden="true"></i></button> --}}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-5">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Izmena profila</h3>
            </div>
            <div class="box-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
                <form action="{{ url('profile', $profile->id) }}" method="post">
                    <input type="hidden" name="_method" value="PATCH">
                    {{ csrf_field() }}
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Ime" name="firstname"
                        value="{{ $profile->firstname }}"/>
                        <span class="fa fa-user fa-lg form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Prezime" name="lastname"
                        value="{{ $profile->lastname }}"/>
                        <span class="fa fa-user fa-lg form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" name="email"
                        value="{{ $profile->email }}"/>
                        <span class="fa fa-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="hidden" class="form-control" placeholder="Avatar" name="image"
                        value="{{ $profile->image }}"/>
                        {{-- <span class="fa fa-picture-o form-control-feedback"></span> --}}
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Prihvati izmene profila</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Promeni lozinku</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('profile/'.$profile->id.'/password') }}" method="post">
                    <input type="hidden" name="_method" value="PATCH">
                    {{ csrf_field() }}
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Lozinka" name="password"
                        value=""/>
                        <span class="fa fa-lock fa-lg form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Ponovite lozinku"
                        name="password_confirmation"
                        value=""/>
                        <span class="fa fa-lock fa-lg form-control-feedback"></span>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Prihvati promenu lozinke</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    @role('admin')
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Uloge Korisnika</h3>
            </div>
            <div class="box-body">
                {!! Form::open(['url' => "/profile/$profile->id/roles"]) !!}
                <table class="table table-hover">
                    @foreach($rol as $role)
                    <tr>
                        <td class="col-md-10">
                            {!! Form::label("$role->name", "$role->display_name"); !!}
                        </td>
                        <td class="col-md-2">
                            {!! Form::radio("role","$role->id", $profile->hasRole("$role->name")); !!}
                        </td>
                    </tr>
                    @endforeach
                </table>
                <hr>
                {!! Form::submit('Аžuriraj uloge', ['class' => 'btn btn-primary form-control']); !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @endrole
</div>

{{-- modal za slanje poruke --}}
{{-- <div id="poruka" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pošalji poruku korisniku {{ $profile->firstname }}</h4>
            </div>
            {!! Form::open(['url' => 'message', 'class' => 'form']) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::text('subject', null, ['placeholder' => 'Subject', 'class' => 'form-control']); !!}
                    {!! Form::hidden('receiver_id', $profile->id) !!}
                </div>
                <div class="form-group">
                {!! Form::textarea('body', null, ['placeholder' => 'Tekst poruke', 'class' => 'form-control']); !!}
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Pošalji </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div> --}}
@endsection
@section('scripts-extra')
<script type="text/javascript">
// function image(img) {
//     var src = img.src;
//     window.open(src);
// }
$('#OpenImgUpload').click(function(){ $('#imgupload').trigger('click'); });
</script>
@endsection