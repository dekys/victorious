@extends('layouts.app')

@section('htmlheader_title')
    Novi korisnik
@endsection

@section('contentheader_title')
    Novi korisnik
@endsection

@section('header-extra')

@endsection

@section('main-content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Novi profil
                </h3>
            </div>
            <div class="box-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <p>
                        {{ $error }}
                    </p>
                    @endforeach
                </div>
                @endif
                {!! Form::open(['route' => 'profile.store', 'class' => '' ]) !!}
                @include('profile._form')  
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts-extra')
<script type="text/javascript">
    $('#OpenImgUpload').click(function(){ $('#imgupload').trigger('click'); });
</script>
@endsection
