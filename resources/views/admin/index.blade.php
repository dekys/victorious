@extends('layouts.app')

@section('htmlheader_title')
    Administracija korisnika
@endsection

@section('contentheader_title')
    Administracija korisnika
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Svi korisnici</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped">
                            <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th></th>
                                <th>Ime</th>
                                <th>Prezime</th>
                                <th>Email adresa</th>
                                <th width="1">Uloga</th>
                                <th width="1"></th>
                            </tr>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td align="center"><img class="img-responsive img-circle" src="{{ $user->image }}" alt="User profile picture" width="50" height="50"></td>
                                    <td>{{ $user->firstname }}</td>
                                    <td>{{ $user->lastname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @foreach($user->roles as $roles)
                                            <p class="text-muted">{{ $roles->display_name }}</p>
                                        @endforeach
                                    </td>
                                    <td style="width: 200px">
                                        <a href="/profile/{{ $user->id }}/edit" class="btn btn-lg" title="Izmeni podatke"><i class="fa fa-pencil-square-o"></i> </a>

                                        <a type="button" class="btn btn-lg" data-toggle="modal" data-target="#deleteUser" data-user_id="{{ $user->id }}" data-user_name="{{ $user->firstname.' '.$user->lastname }}" title="Obriši korisnika"><i class="fa fa-trash-o"></i> </a>    

                                        <a type="button" class="btn btn-lg" data-toggle="modal" data-target="#sendMessage" data-user_id="{{ $user->id }}" data-user_name="{{ $user->firstname}}" title="Pošalji poruku"><i class="fa fa-envelope-o"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="text-center">
                            {!! $users->links(); !!}                
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

<div id="deleteUser" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Brisanje</h4>
      </div>
      <div class="modal-body">
        <p>Da li ste sigurni da želite obrisati korisnika <span id="user"></span></p>
      </div>
      <div class="modal-footer">
        {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
        <button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
        {!! Form::submit('Obriši', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
      </div>
    </div>

  </div>
</div>

<div id="sendMessage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pošalji poruku korisniku <span id="receiver"></span></h4>
            </div>
            {!! Form::open(['url' => 'message', 'class' => 'form']) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::text('subject', null, ['placeholder' => 'Subject', 'class' => 'form-control']); !!}
                    {!! Form::hidden('receiver_id', null, ['id' => 'receiver_id']) !!}
                </div>
                <div class="form-group">
                {!! Form::textarea('body', null, ['placeholder' => 'Tekst poruke', 'class' => 'form-control']); !!}
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Pošalji </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection

@section('scripts-extra')

    <script type="text/javascript">
        $('#deleteUser').on('show.bs.modal', function(e) {
            var user_id = $(e.relatedTarget).data('user_id');
            var user_name = $(e.relatedTarget).data('user_name');
            $("#delForm").attr('action', 'profile/'+user_id);
            $("#user").text(user_name);
        });

        $(document).ready(function() {
            $('#example').DataTable({
                "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
                }
            });
        } );

         $('#sendMessage').on('show.bs.modal', function(e) {
            var user_id = $(e.relatedTarget).data('user_id');
            var user_name = $(e.relatedTarget).data('user_name');
            $("#receiver_id").val(user_id);
            $("#receiver").text(user_name);
        });

    </script>

@endsection