@extends('layouts.app')

@section('htmlheader_title')
    Prikaz polaznika termina
@endsection

@section('contentheader_title')
    Prikaz polaznika termina
@endsection

@section('header-extra')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css"/>
@endsection

@section('main-content')
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><a href="{{ route('course.show', $event->course->id) }}"  class="btn btn-md" title="Prikaz trmina">{{$event->course->service->naziv}}</a></h3>
                        <p>Dana {{Carbon::parse($event->start)->format('d.m.Y.') }} godine u periodu od {{Carbon::parse($event->start)->format('H:i') }} do {{Carbon::parse($event->end)->format('H:i') }}</p>
                    </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="example" class="table table-hover table-condensed table-striped">
                                <thead>
                                    <th>R.br.</th>
                                    <th>Ime i prezime</th>
                                    <th>Prisutan</th>
                                </thead>

                                <tbody>

                                    @php
                                        $i = ($students->currentPage()-1)*$students->perPage();
                                    @endphp

                                        @foreach ($students as $student)
                                            <tr>
                                                <td> {{ ++$i }}.</td>
                                                <td> <a href="{{ route('student.show', $student->id) }}" title="Prikaži detalje">{{ $student->ime }} {{ $student->prezime }}</a></td>
                                                <td class="td" align="left" style="width:150px"> 
                                                    {{Form::checkbox('chk_'.$student->id, '1', $event->students->contains('id',$student->id), ['id'=>"$student->id", 'onchange'=>"toggleCheckbox(this)"])}}
                                                </td>
                                            </tr>
                                        @endforeach

                                </tbody>  

                            </table>

                            <div class="text-center">
                                {!! $students->links(); !!}                
                            </div>

                        </div>
                    </div>
            <!-- D ugmici -->
                {{-- <div class="row"> --}}
                        <div class="row">
                            <div class="col-md-12 col-md-offset-3">
                                <div class="modal-footer">

                                    <div class="col-md-3">
                                        {{ Form::button('Izbriši', array('class' => 'btn btn-danger  btn-block', 'type'=>'reset')) }} 
                                    </div>
                                    
                                    @if (isset($event->realized) )
                                        <div class="col-md-3">
                                            {{ Form::button('Označi kao ne realizovano', array('class' => 'btn btn-primary  btn-block', 'type'=>'reset')) }} 
                                        </div>
                                    @else
                                        <div class="col-md-3">
                                            {{ Form::button('Označi kao realizovano', array('class' => 'btn btn-primary  btn-block', 'type'=>'reset')) }} 
                                        </div>
                                    @endif


                            <div class="col-md-3">
                                {{ Form::button('Pošalji SMS', array('class' => 'btn btn-primary  btn-block', 'type'=>'reset')) }} 
                            </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </div> 
@endsection

@section('scripts-extra')
    <script type="text/javascript">

        function toggleCheckbox(element)
        {
            $.ajax({
                type:'POST',
                url: '{{ url('event/attend') }}',
                data:{
                        'present' :element.checked,
                        'event_id':{{$event->id}},
                        'student_id':element.id,
                        '_token': $('input[name=_token]').val()
                },
                success:function(data){
                    if (element.checked) {

                        toastr.success('Polaznink je označen kao prisutan.', '', {positionClass: 'toast-bottom-right'})

                    } else {

                        toastr.success('Polaznik nije prisutan.', '', {positionClass: 'toast-bottom-right'})
                        
                    }
                },
                error:function(){
                    console.log('neuspeh')
                }
            });
        }
    </script>
@endsection