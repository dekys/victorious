    <!-- Unos podataka o uceniku -->

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Kurs</h3>
            </div>
            <div class="box-body">
                <div class="row col-md-12">
                    <div class="row">
                        <div class="col-sm-4">
                            {{-- {!! Form::label('ime', 'Ime * :') !!} --}}
                            {{ Form::text('naziv', null, 
                                [
                                'class'                         => 'form-control form-group',
                                'placeholder'                   => 'Naziv usluge',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      => '',  
                                //'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                'data-parsley-minlength'        => '5',
                                'data-parsley-maxlength'        => '50'
                                ])
                            }} 
                        </div>  

                    </div>
                </div>

                <div class="row col-md-12">
                    <div class="row">
                        <div class="col-sm-12">
                            {{-- {!! Form::label('rodjen', 'Datum rođenja * :') !!} --}}
                            {{ Form::textarea('opis',  null,  
                                [
                                'resize'                        => 'none',
                                'size'                          => '30x5',
                                'data-parsley-required-message' => 'Ovo polje je obavezno',
                                'class'                         => 'form-control form-group', 
                                 'placeholder'                  => 'Opis usluge',
                                ])
                            }} 
                        </div>
                    </div>
                </div>

                <div class="row col-md-12">     
                    <div class="row">
                        <div class="col-sm-4">
                            {{-- {{ Form::label('adresa', 'Adresa prebivališta:') }} --}}
                            {{ Form::number('cena', null, 
                                [
                                'class'                         => 'form-control',
                                'step'                          =>'any',
                                'placeholder'                   =>'Cena usluge',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'', 
                                ])
                            }}
                        </div>

                        <div class="col-sm-4">
                            {{-- {{ Form::label('telefon', 'Kontakt telefon:') }} --}}
                            {{ Form::number('trajanje', null, 
                                [
                                'class'                         =>'form-control form-group', 
                                'placeholder'                   =>'Trajanje usluge',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'required'                      =>'',
                                'maxlength'                     =>"50"
                                ]) 
                            }}
                        </div>  

                        <div class="col-sm-4">
                            {{-- {{ Form::label('email', 'Email adresa:') }} --}}
                            {{ Form::text('jm', null, 
                                [
                                'class'                         => 'form-control form-group', 
                                'placeholder'                   =>'Jedinica mere - časova, dana, sati',
                                'data-parsley-required-message' => 'Ovo polje je obavezno', 
                                'data-type-email-message'       =>'Unesite validnu email adresu',
                                'required'                      =>'',
                                ]) 
                            }}
                        </div>  
                    </div>
                </div>       

               
                <!-- Dugmici -->
                <div class="row">
                    <div class="col-md-12 col-md-offset-8">
                        <div class="row">
                            <div class="col-md-2">
                                {{ Form::button('Poništi', array('class' => 'btn btn-primary  btn-block', 'type'=>'reset')) }} 
                            </div>
                            <div class="col-md-2">
                                {{ Form::button('Prihvati',  array('class' => 'btn btn-primary btn-block', 'type'=>'submit')) }} 
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- end of .row dugmici-->
            </div> 
        </div> 
    </div>
</div> <!-- end of .row -->

   
