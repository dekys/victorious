<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title> <?php echo $__env->yieldContent('htmlheader_title', 'Your title here'); ?> </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo e(asset('/css/bootstrap.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="<?php echo e(asset('/css/AdminLTE.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skin (Blue) -->
    <link href="<?php echo e(asset('/css/skins/skin-blue.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- iCheck -->
    <link href="<?php echo e(asset('/plugins/iCheck/square/blue.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Toastr -->
    <link href="<?php echo e(asset('/css/toastr.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- SweetAlert2 -->
    

    <link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="<?php echo e(asset('img/favicons/favicon.ico')); ?>" >
    

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    

<?php echo $__env->yieldContent('header-extra'); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo e(url('/home')); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><?php echo e(Html::image('/img/logo.png', '', array( 'height' => '20px' ))); ?></span>
            <!-- logo for regular state and mobile devices -->
            
            <span class="logo-lg"><?php echo e(Html::image('/img/logo.png', '', array( 'height' => '45px' ))); ?> </span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-danger"><?php echo e($msg_cnt); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Imate <?php echo e($msg_cnt); ?> nove poruke</li>
                            <li>
                                <!-- inner menu: contains the messages -->
                                <ul class="menu">
                                    <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <li><!-- start message -->
                                        <a type="button" 
                                        href="#" 
                                        data-toggle="modal" 
                                        data-target="#msgModal" 
                                        data-sender="<?php echo e($message->sender->firstname); ?> <?php echo e($message->sender->lastname); ?>" 
                                        data-body ="<?php echo e($message->body); ?>"
                                        data-subject ="<?php echo e($message->subject); ?>"
                                        data-msgid ="<?php echo e($message->id); ?>"

                                            >
                                            <div class="pull-left">
                                                <!-- User Image -->
                                                <img src="<?php echo e($message->sender->image); ?>" class="img-circle"
                                                     alt="User Image"/>
                                            </div>
                                            <!-- Message title and timestamp -->
                                            <h4>
                                                <?php echo e($message->sender->firstname); ?> <?php echo e($message->sender->lastname); ?>

                                                <small><i class="fa fa-clock-o"></i> <?php echo e($message->created_at->diffForHumans()); ?></small>
                                            </h4>
                                            <!-- The message -->
                                            <p><?php echo e($message->subject); ?></p>
                                        </a>
                                    </li><!-- end message -->
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </ul><!-- /.menu -->
                            </li>
                            <li class="footer"><a href="/message">Pogledaj sve poruke</a></li>
                        </ul>
                    </li><!-- /.messages-menu -->

                    
                    <?php if(Auth::guest()): ?>
                        <li><a href="<?php echo e(url('/login')); ?>">Login</a></li>
                        <li><a href="<?php echo e(url('/register')); ?>">Register</a></li>
                <?php else: ?>
                    <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="<?php echo e(asset(Auth::user()->image)); ?>" class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs"><?php echo e(Auth::user()->firstname." ".Auth::user()->lastname); ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="<?php echo e(url(asset(Auth::user()->image))); ?>" class="img-circle" alt="User Image"/>
                                    <p>
                                        <?php echo e(Auth::user()->firstname." ".Auth::user()->lastname); ?>

                                        <small><?php echo e(Auth::user()->created_at->diffForHumans()); ?></small>
                                    </p>
                                </li>
                               
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo e(url('profile')); ?>" class="btn btn-default btn-flat">Profil</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo e(url('/logout')); ?>" class="btn btn-default btn-flat"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout</a>

                                        <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST"
                                              style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                <?php endif; ?>

                <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-calendar-o"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <?php if(! Auth::guest()): ?>
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo e(asset(Auth::user()->image)); ?>" class="img-circle" alt="User Image"/>
                    </div>
                    <div class="pull-left info">
                        <p><?php echo e(Auth::user()->firstname." ".Auth::user()->lastname); ?></p>
                        <!-- Status -->
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
        <?php endif; ?>




        <!-- search form (Optional) -->
            <form action="" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Pretraga..."/>
                    <span class="input-group-btn">
                        <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
            <!-- /.search form -->


            <ul class="sidebar-menu">
            

                <li class="<?php echo e(Request::is('dashboard*') ? 'active' : ''); ?>"><a href="/home"><i class="fa fa-home"></i><span>POČETNA</span></a>
                </li>

                <li class="treeview <?php echo e(Request::is('administrator/student*') ? 'active' : ''); ?>"><a href="#"><i class="fa fa-graduation-cap"></i><span>POLAZNICI</span></a>
                    <ul class='treeview-menu'>
                        <li class="<?php echo e(Request::is('administrator/student') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/student')); ?>"><span>Pregled svih polaznika</span></a></li>
                        <li class="<?php echo e(Request::is('administrator/student/create*') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/student/create')); ?>"><span>Unos novog polaznika </span></a></li>
                    </ul>
                </li>

                
                <li class="treeview <?php echo e(Request::is('administrator/teacher*') ? 'active' : ''); ?>"><a href="#"><i class="fa fa-users"></i><span>PREDAVAČI</span></a>
                    <ul class='treeview-menu'>
                        <li class="<?php echo e(Request::is('administrator/teacher') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/teacher')); ?>"><span>Pregled svih predavača</span></a></li>
                        <li class="<?php echo e(Request::is('administrator/teacher/create*') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/teacher/create')); ?>"><span>Unos novog predavača </span></a></li>
                    </ul>
                </li>

                
                <li class="treeview <?php echo e(Request::is('administrator/service*') || Request::is('administrator/category*') ? 'active' : ''); ?>"><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>USLUGE</span></a>
                    <ul class='treeview-menu'>
                        <li class="<?php echo e(Request::is('administrator/service') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/service')); ?>"><span>Pregled svih usluga</span></a></li>
                        <li class="<?php echo e(Request::is('administrator/service/create*') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/service/create')); ?>"><span>Unos nove usluge</span></a></li>
                        <li class="<?php echo e(Request::is('administrator/category') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/category')); ?>"><span>Kategorije</span></a></li>
                    </ul>
                </li>

                
                <li class="treeview <?php echo e(Request::is('administrator/course*') ? 'active' : ''); ?>"><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i><span>TERMINI</span></a>
                    <ul class='treeview-menu'>
                        <li class="<?php echo e(Request::is('administrator/course') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/course')); ?>"><span>Pregled svih termina</span></a></li>
                        <li class="<?php echo e(Request::is('administrator/course/create*') ? 'active' : ''); ?>"><a href="<?php echo e(url('administrator/course/create')); ?>"><span>Unos novog termina</span></a></li>
                    </ul>
                </li>
                
                <?php if (\Ntrust::hasRole('admin')) : ?>
                    <li class="header">PODEŠAVANJA</li>

                    
                     <li class="treeview <?php echo e(Request::is('admin') ? 'active' : ''); ?>"><a href="#"><i class="fa fa-user" aria-hidden="true"></i><span>KORISNICI</span></a>
                        <ul class='treeview-menu'>
                            <li ><a href="<?php echo e(url('admin')); ?>"><span>Pregled svih korisnika</span></a></li>
                            <li class="<?php echo e(Request::is('admin*') ? 'active' : ''); ?>"><a href="<?php echo e(url('profile/create')); ?>"><span>Unos novog korisnika </span></a></li>
                        </ul>
                    </li>
                    
                     <li class="treeview <?php echo e(Request::is('location*') ? 'active' : ''); ?>"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><span>LOKACIJE</span></a>
                        <ul class='treeview-menu'>
                            <li ><a href="<?php echo e(url('location')); ?>"><span>Pregled svih lokacija</span></a></li>
                            <li class="<?php echo e(Request::is('location/create') ? 'active' : ''); ?>"><a href="<?php echo e(url('location/create')); ?>"><span>Unos novih lokacija </span></a></li>
                        </ul>
                    </li>
                <?php endif; // Ntrust::hasRole ?>

 
            </ul><!-- /.sidebar-menu -->
            
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $__env->yieldContent('contentheader_title', 'Page Header here'); ?>
                <small><?php echo $__env->yieldContent('contentheader_description'); ?></small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            <?php echo $__env->yieldContent('main-content'); ?>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-pencil"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-calendar-o"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Predstojeće aktivnosti</h3>
                <ul class='control-sidebar-menu'>
                    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <li>
                        <a href="/event/<?php echo e($event->id); ?>">
                            <i class="menu-icon fa fa-pencil bg-blue"></i>
                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading"><?php echo e($event->title); ?></h4>
                                <p><?php echo e(Carbon\Carbon::parse($event->start)->diffForHumans()); ?></p>
                            </div>
                        </a>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </ul><!-- /.control-sidebar-menu -->
            </div><!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>
                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked/>
                        </label>
                        <p>
                            Some information about this general settings option
                        </p>
                    </div><!-- /.form-group -->
                </form>
            </div><!-- /.tab-pane -->
        </div>
    </aside><!-- /.control-sidebar -->

    <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    <div class='control-sidebar-bg'></div>

    <div id="msgModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Poruka od <span id="from"></span></h4>
      </div>
      <div class="modal-body">
        <h3 id="subject"></h3>
        <p id="body"></p>
      </div>
      <div class="modal-footer">
         <?php echo Form::open(['method' => 'PATCH', 'id'=>'msgRead']); ?>

        <?php echo Form::submit('Označi kao pročitano', ['class' => 'btn btn-primary']); ?>

        <button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
        <?php echo e(Form::close()); ?>

      </div>
    </div>

  </div>
</div>

</div><!-- ./wrapper -->

<?php echo $__env->make('layouts.partials.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">

         $('#msgModal').on('show.bs.modal', function(e) {
            var from = $(e.relatedTarget).data('sender');
            var subject = $(e.relatedTarget).data('subject');
            var body = $(e.relatedTarget).data('body');
            var message_id = $(e.relatedTarget).data('msgid');

            $("#from").text(from);
            $("#subject").text(subject);
            $("#body").text(body);
            $("#msgRead").attr('action', '/message/'+message_id);
        });

    </script>


</body>
</html>