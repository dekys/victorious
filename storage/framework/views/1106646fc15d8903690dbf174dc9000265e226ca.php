<?php $__env->startSection('htmlheader_title'); ?>
    Kalendar 
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
    Kalendar
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-extra'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css"/>

    <style>
        #calendar {
            height: 80%;
        }
    </style>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

<div>
    <div class="btn-group" role="group" aria-label="...">
        <ul class="nav nav-pills" id="rowTab">
            <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                
                <a href="<?php echo e($location->id); ?>" class="btn btn-default btn-sm <?php echo e(Request::is("dashboard/$location->id") ? 'active' : ''); ?>""><?php echo e($location->name); ?></a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </ul>
    </div>

 
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                     <div class="tab-content">
                        <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <div id=<?php echo e($location->id); ?> class="tab-pane fade in active">
                                <div id="calendar" class="fc fc-unthemed fc-ltr"> </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts-extra'); ?>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="<?php echo e(asset('plugins/fullcalendar/locale/sr.js')); ?>" type="text/javascript"></script>

    <script>

        $(document).ready(function() {

           var $calendar = $('#calendar').fullCalendar(
            {   
                snapDuration: '00:15:00',
                editable: true,
                minTime: "07:00:00",
                maxTime: "22:00:00",
                contentHeight: 800,
                header: {
                    left: 'today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay prev,next'
                    },
                defaultView: 'agendaWeek',
                events: [

                        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        {
                            id : '<?php echo e($event->id); ?>',
                            title : '<?php echo e($event->title); ?>',
                            start : '<?php echo e($event->start); ?>',
                            end : '<?php echo e($event->end); ?>',
                            description : '<?php echo e(@isset ($event->course->teacher->ime)  ? $event->course->teacher->ime .' '. $event->course->teacher->prezime : ""); ?>',
                            url: '<?php echo e(url('/event')); ?>' + '/' + '<?php echo e($event->id); ?>',
                            color: '<?php echo e($event->background_color); ?>',
                            textColor: '#222d32',
                        },
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                ],
                    eventRender: function(event, element) { 
                         element.find('.fc-title').append("<br/>" + event.description); 
                     },

                eventResize: function(event, delta, revertFunc) {
                    start = moment(event.start, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
                    end = moment(event.end, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')

                    $.ajax({
                        type: "POST",
                        url: '<?php echo e(url('/event')); ?>' + '/' + event.id,
        
                        data: { 
                            'id': event.description,
                            'title': event.title,
                            'start': start,
                            'end': end,
                            '_method': 'put',
                            'dataType': 'json',
                            '_token': $('input[name=_token]').val()
                        }
                    })
                },



                eventDrop: function(event, delta, revertFunc) {
                    start = moment(event.start, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')
                    end = moment(event.end, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss')

                    $.ajax({
                        type: "POST",
                        url: '<?php echo e(url('/event')); ?>' + '/' + event.id,
        
                        data: { 
                            'id': event.description,
                            'title': event.title,
                            'start': start,
                            'end': end,
                            '_method': 'put',
                            'dataType': 'json',
                            '_token': $('input[name=_token]').val()
                        }
                    })
                }
            });
        });

    </script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>