<?php $__env->startSection('htmlheader_title'); ?>
    Administracija korisnika
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
    Administracija korisnika
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Svi korisnici</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover table-condensed table-striped">
                            <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th></th>
                                <th>Ime</th>
                                <th>Prezime</th>
                                <th>Email adresa</th>
                                <th width="1">Uloga</th>
                                <th width="1"></th>
                            </tr>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <tr>
                                    <td><?php echo e($user->id); ?></td>
                                    <td align="center"><img class="img-responsive img-circle" src="<?php echo e($user->image); ?>" alt="User profile picture" width="50" height="50"></td>
                                    <td><?php echo e($user->firstname); ?></td>
                                    <td><?php echo e($user->lastname); ?></td>
                                    <td><?php echo e($user->email); ?></td>
                                    <td>
                                        <?php $__currentLoopData = $user->roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $roles): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <p class="text-muted"><?php echo e($roles->display_name); ?></p>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </td>
                                    <td style="width: 200px">
                                        <a href="/profile/<?php echo e($user->id); ?>/edit" class="btn btn-lg" title="Izmeni podatke"><i class="fa fa-pencil-square-o"></i> </a>

                                        <a type="button" class="btn btn-lg" data-toggle="modal" data-target="#deleteUser" data-user_id="<?php echo e($user->id); ?>" data-user_name="<?php echo e($user->firstname.' '.$user->lastname); ?>" title="Obriši korisnika"><i class="fa fa-trash-o"></i> </a>    

                                        <a type="button" class="btn btn-lg" data-toggle="modal" data-target="#sendMessage" data-user_id="<?php echo e($user->id); ?>" data-user_name="<?php echo e($user->firstname); ?>" title="Pošalji poruku"><i class="fa fa-envelope-o"></i> </a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </tbody>
                        </table>

                        <div class="text-center">
                            <?php echo $users->links();; ?>                
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

<div id="deleteUser" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Brisanje</h4>
      </div>
      <div class="modal-body">
        <p>Da li ste sigurni da želite obrisati korisnika <span id="user"></span></p>
      </div>
      <div class="modal-footer">
        <?php echo Form::open(['method' => 'DELETE', 'id'=>'delForm']); ?>

        <button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
        <?php echo Form::submit('Obriši', ['class' => 'btn btn-danger']); ?>

        <?php echo Form::close(); ?>

      </div>
    </div>

  </div>
</div>

<div id="sendMessage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pošalji poruku korisniku <span id="receiver"></span></h4>
            </div>
            <?php echo Form::open(['url' => 'message', 'class' => 'form']); ?>

            <div class="modal-body">
                <div class="form-group">
                    <?php echo Form::text('subject', null, ['placeholder' => 'Subject', 'class' => 'form-control']);; ?>

                    <?php echo Form::hidden('receiver_id', null, ['id' => 'receiver_id']); ?>

                </div>
                <div class="form-group">
                <?php echo Form::textarea('body', null, ['placeholder' => 'Tekst poruke', 'class' => 'form-control']);; ?>

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Pošalji </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts-extra'); ?>

    <script type="text/javascript">
        $('#deleteUser').on('show.bs.modal', function(e) {
            var user_id = $(e.relatedTarget).data('user_id');
            var user_name = $(e.relatedTarget).data('user_name');
            $("#delForm").attr('action', 'profile/'+user_id);
            $("#user").text(user_name);
        });

        $(document).ready(function() {
            $('#example').DataTable({
                "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Serbian.json"
                }
            });
        } );

         $('#sendMessage').on('show.bs.modal', function(e) {
            var user_id = $(e.relatedTarget).data('user_id');
            var user_name = $(e.relatedTarget).data('user_name');
            $("#receiver_id").val(user_id);
            $("#receiver").text(user_name);
        });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>