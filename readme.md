# Victorious
App for school management

# Requirements
+ php 7.1.6
+ mySQL server
+ Composer PHP dependacy manager

# Installation

1. Clone the project into a folder of your choice with
   "git clone https://kordarei@bitbucket.org/dekys/victorious.git ."
2. Through composer run "composer install"
3. (To get the latest packages, run composer update)
4. Edit the .env.example file with your database information and save it as .env
5. Run "php artisan key:generate" to generate a key for your application
6. Run "php artisan migrate --seed" to add a SuperAdmin user

### SuperAdmin user:
+ Email: admin@victorious.co.rs
+ Password: victorious
